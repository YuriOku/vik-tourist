
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Yuriy Shirokikh
 */
public class MsSql {
    // JDBC driver name and database URL
    final String JDBC_DRIVER="com.microsoft.sqlserver.jdbc.SQLServerDriver";
//    final String DB_URL="jdbc:sqlserver://82.144.203.20:1433;databaseName=uto;user=sa;password=DYK6PPaeEa";
    final String DB_URL="jdbc:sqlserver://89.252.5.10:1433;databaseName=uto;user=sa;password=DYK6PPaeEa";
    
    // Declare the JDBC objects.
    private Connection con = null;
    Statement stmt = null;
    
    public MsSql() {
        try {
            Class.forName(JDBC_DRIVER);
            this.init();
        } catch (ClassNotFoundException e) {
            
        }
    }
    
    private void init() {
        try {
            // Establish the connection.
            con = DriverManager.getConnection(DB_URL);
            // Create and execute an SQL statement that returns some data.
            stmt = con.createStatement();
            Logger.getLogger(MsSql.class.getName()).log(Level.SEVERE, "Open connection and get statement");
        } catch (SQLException ex) {
            Logger.getLogger(MsSql.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }
    
    public Boolean getStatus() throws SQLException {
        if( con.isClosed() ){
            return false;
        } else {
            return true;
        }
    }
    
    public Boolean renewStatus() throws SQLException{
        this.init();
        return true;
    }
    
    public ResultSet executeQuery(String sql) throws SQLException {
        return stmt.executeQuery(sql);
    }
    
    public void destroy() {
        if (stmt != null) try { 
            stmt.close();
        } catch(Exception e) {}
        if (con != null) try {
            Logger.getLogger(MsSql.class.getName()).log(Level.SEVERE, "Close connection and statement");
            con.close();
        } catch(Exception e) {}
    }
}
