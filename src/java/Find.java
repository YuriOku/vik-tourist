/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;


class TableMask {
    public String table_name = "";
    public String _Fld52988RRef = "";
    public String _Fld52988RRef_1 = "";
    public String _Fld52989 = "";
    public String _Fld52990 = "";
    public String _Fld52991RRef = "";
    public String _Fld52992RRef = "";
    public String _Fld52993RRef = "";
    public String _Fld52994RRef = "";
    public String _Fld52995 = "";
    public String newEatField = "";

    @Override
    public String toString() {
        return " | "+ table_name +" | "+ _Fld52988RRef +" | "+ _Fld52988RRef_1 +" | "+ _Fld52989 +" | "+ _Fld52990 +" | "+ _Fld52991RRef +" | "+ _Fld52992RRef +" | "+ _Fld52993RRef +" | "+ _Fld52994RRef +" | "+ _Fld52995 +" | ";
    }
    
}

/**
 *
 * @author Yurchik
 */
public class Find extends HttpServlet {
    private MsSql db;
    private Template Sql = null;
    private ResultSet rs = null;

    private String type;
    private String country;
    private int wAvia;
    private String city;
    private String spo;
    private String ttour;
    private String dateFrom;
    private String dateTo;
    private Date dateFromD;
    private Date dateToD;
    private int adult;
    private int child;
    private String nightsFrom;
    private String nightsTo;
    private String earlyBron;
    private String stopSale;
    private String mRo;
    private String mBb;
    private String mHb;
    private String mHbplus;
    private String mFb;
    private String mFbplus;
    private String mAi;
    private String mUai;
    private String hSecond;
    private String hThird;
    private String hFourth;
    private String hFifth;
    private String hApt;
    private String hVilla;
    private int priceFrom;
    private int priceTo;
    private String currency;
    String[] resorts;
    String[] hotels;
    
    private JSONObject full = new JSONObject();
    
    private String CTCVID = "";
    private String pusto = "0x00000000000000000000000000000000";
    Ratio rt = Ratio.getInstance();

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        db = new MsSql();
        Helper.setCur("eur", "€");
        Helper.setCur("usd", "$");
    }
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp); //To change body of generated methods, choose Tools | Templates.
        try {
            if( !db.getStatus() ){
                db.renewStatus();
                Logger.getLogger(GetCountry.class.getName()).log(Level.SEVERE, "Connection closed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetCountry.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
        db.destroy();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Find</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Find at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    

    protected TableMask getTableMask(int wAvia,int adult,int child) throws IOException, SQLException{
        
        TableMask mask = new TableMask();
        
        Sql = new Template(getServletConfig(), "query/getTableMask.sql");
        Sql.parse("wAvia", Integer.toString(wAvia));
        Sql.parse("Adult", Integer.toString(adult));
        Sql.parse("Child", Integer.toString(child));
        if(!Sql.isEmpty()){
            rs = db.executeQuery(Sql.get());
            while (rs.next()) {
                String val = rs.getString("isValue");
                switch (rs.getString("tKey")) {
                    case "table_name":{ mask.table_name = val; break; }
                    case "_Fld52988RRef":{ mask._Fld52988RRef = val; break; }
                    case "_Fld52988RRef_1":{ mask._Fld52988RRef_1 = val; break; }
                    case "_Fld52989":{ mask._Fld52989 = val; break; }
                    case "_Fld52990":{ mask._Fld52990 = val; break; }
                    case "_Fld52991RRef":{ mask._Fld52991RRef = val; break; }
                    case "_Fld52992RRef":{ mask._Fld52992RRef = val; break; }
                    case "_Fld52993RRef":{ mask._Fld52993RRef = val; break; }
                    case "_Fld52994RRef":{ mask._Fld52994RRef = val; break; }
                    case "_Fld52995":{ mask._Fld52995 = val; break; }
                    case "newEatField":{ mask.newEatField = val; break; }
                }
            }
        }
        return mask;
    }
    
    protected void getCTCVID() throws IOException {
        if( wAvia == 1 ){
            Sql = new Template(getServletConfig(), "query/getCTCVID_New.sql");
            Sql.parse("country", Helper.getBinar(country));
            Sql.parse("city", Helper.getBinar(city));
        } else {
            Sql = new Template(getServletConfig(), "query/getCTCVID.sql");
            Sql.parse("country", Helper.getBinar(country));
            Sql.parse("city", Helper.getBinar(city));
        }
        
        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                List<String> strings = new LinkedList<>();
                while (rs.next()) {
                    strings.add(Helper.getBinar(rs.getString("_IDRRef")));
                }
                CTCVID = String.join(",", strings);
                
            } catch (Exception e) {
//                out.println(e.getMessage());
            }
        }
    }
    
    protected void fillRatio() throws IOException, SQLException{
        Template coefRangeSql = new Template(getServletConfig(), "query/getCoefRange.sql");
        coefRangeSql.parse("country", Helper.getBinar(country));
        rt.setColumnNumber(db.executeQuery(coefRangeSql.get()), Integer.parseInt(nightsFrom));
        
        if(Ratio.getInstance().getColumnNumber() != 0){
            Template coefItemsSql = new Template(getServletConfig(), "query/getCoefItems.sql");
            coefItemsSql.parse("country", Helper.getBinar(country));
            coefItemsSql.parse("dateFrom", Helper.getWYDate(dateFromD, new SimpleDateFormat("yyyyMMdd")));
            coefItemsSql.parse("dateTo", Helper.getWYDate(dateToD, new SimpleDateFormat("yyyyMMdd")));
            rt.setCoefItem(db.executeQuery(coefItemsSql.get()));
        }
    }
    
    protected String getNighter(){
        Fltr nList = new Fltr();
        for (int i = 1; i < 30; i++) {
            if( i >= Integer.parseInt(nightsFrom) && i <= Integer.parseInt(nightsTo) ){
                nList.add(i);
            }
        }
        return nList.make(",");
    }
    
    protected String getHotels(){
        Fltr nList = new Fltr();
        for(int i = 0;i < hotels.length;i++){
            nList.add(Helper.getBinar(hotels[i]));
        }
        return nList.make(",");
    }
    
    protected boolean isEat(){
        if( mRo.equals("true") || mBb.equals("true") || mHb.equals("true") || mHbplus.equals("true") || mFb.equals("true") || mFbplus.equals("true") || mAi.equals("true") || mUai.equals("true") ){
            return true;
        } else {
            return false;
        }
    }
    
    protected String getEat(){
        Fltr nList = new Fltr();
        if(mRo.equals("true")){ nList.add("1"); }
        if(mBb.equals("true")){ nList.add("2"); }
        if(mHb.equals("true")) { nList.add("3"); }
        if(mHbplus.equals("true")) { nList.add("4"); }
        if(mFb.equals("true")) { nList.add("5"); }
        if(mFbplus.equals("true")) { nList.add("6"); }
        if(mAi.equals("true")) { nList.add("7"); }
        if(mUai.equals("true")) { nList.add("8"); }
        return nList.make(",");
    }
    
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        type = request.getParameter("type");
        country = request.getParameter("country");
        wAvia = Integer.parseInt(request.getParameter("wAvia"));
        city = request.getParameter("city");
        spo = request.getParameter("spo");
        ttour = request.getParameter("ttour");
        dateFrom = request.getParameter("dateFrom");
        dateTo = request.getParameter("dateTo");
        adult = Integer.parseInt(request.getParameter("adult"));
        child = Integer.parseInt(request.getParameter("child"));
        nightsFrom = request.getParameter("nightsFrom");
        nightsTo = request.getParameter("nightsTo");
        earlyBron = request.getParameter("earlyBron");
        stopSale = request.getParameter("stopSale");
        mRo = request.getParameter("mRo");
        mBb = request.getParameter("mBb");
        mHb = request.getParameter("mHb");
        mHbplus = request.getParameter("mHbplus");
        mFb = request.getParameter("mFb");
        mFbplus = request.getParameter("mFbplus");
        mAi = request.getParameter("mAi");
        mUai = request.getParameter("mUai");
        hSecond = request.getParameter("hSecond");
        hThird = request.getParameter("hThird");
        hFourth = request.getParameter("hFourth");
        hFifth = request.getParameter("hFifth");
        hApt = request.getParameter("hApt");
        hVilla = request.getParameter("hVilla");
        priceFrom = Integer.parseInt(request.getParameter("priceFrom"));
        priceTo = Integer.parseInt(request.getParameter("priceTo"));
        currency = request.getParameter("currency");
        resorts = request.getParameterValues("resorts[]");
        hotels = request.getParameterValues("hotels[]");
        
        String countryCourse = "";
        
        full.clear();
        
        PrintWriter out = response.getWriter();
        
        Fltr where = new Fltr();
        
        getCTCVID();
        SimpleDateFormat dFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            dateFromD = dFormat.parse(dateFrom);
            dateToD = dFormat.parse(dateTo);
        } catch (ParseException ex) {
            full.put("error", "Error 002010");
            log(ex.getMessage());
        }
        try {
            fillRatio();
        } catch (SQLException ex) {
            Logger.getLogger(Find.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        try {
            JSONObject list = new JSONObject();
            
            TableMask mask = getTableMask(wAvia, adult, child);
            
            // access in web
            where.add("(T25._Fld52345 = 0x01)");
            where.add("(T1."+ mask._Fld52995 +" > 0)");
            if(wAvia == 1){
                where.add("(T25._Fld50911 = 0x01)");
            }
            // put date range parameter
            String dateFromS = Helper.getWYDate(dateFromD, new SimpleDateFormat("yyyyMMdd"));
            String dateToS = Helper.getWYDate(dateToD, new SimpleDateFormat("yyyyMMdd"));
            where.add("((T1."+ mask._Fld52990 +" >= '"+ dateFromS +"') AND (T1."+ mask._Fld52990 +" <= '"+ dateToS +"'))");
            
            Sql = new Template(getServletConfig(), "query/mainQuery.sql");
            Sql.parse("CCTVWhere","(T1."+ mask._Fld52988RRef +" in ("+ CTCVID +"))");
            // generate nights
            where.add("(T1."+ mask._Fld52989 +" in("+ getNighter() +"))");
            
            // make spo
            if( !spo.equals("0") ){
                where.add("(T25._ParentIDRRef = "+ Helper.getBinar(spo) +")");
            }
            // make type
            if( !ttour.equals("0") ){
                where.add("((((T25._Fld49580RRef IN ("+Helper.getBinar(ttour)+")) OR (T25._Fld53047RRef IN ("+Helper.getBinar(ttour)+"))) OR (T25._Fld53048RRef IN ("+Helper.getBinar(ttour)+"))) OR (T25._Fld53049RRef IN ("+Helper.getBinar(ttour)+")) OR (T25._Fld53081RRef IN ("+Helper.getBinar(ttour)+")))");
            }
            
            // check early brone
            if( earlyBron.equals("true") ){
                where.add("(T25._Fld50902 = 0x01)");
            }
            
            if( stopSale.equals("true") ){
                where.add("(isnull(T333._Fld54311,0x00) = 0x00 AND isnull(T333._Fld54313,0x00) = 0x00 AND isnull((CASE WHEN (((T9._Fld53910 = 0x01 OR T11._Fld53098 = 0x01) OR T12._Fld53903 = 0x01) OR T13._Fld53895 = 0x01) THEN 0x01 WHEN NOT (((T9._Fld53910 = 0x01 OR T11._Fld53098 = 0x01) OR T12._Fld53903 = 0x01) OR T13._Fld53895 = 0x01) THEN 0x00 END),0x00) = 0x00)");
                // с этим не выводит вообще ничего
                //where.add("(T333._Fld54310 > 0) and (T333._Fld54312 > 0)");
            }
            
            // work with price
            if( priceFrom != 0 || priceTo != 0 ){
                if(priceFrom != 0){
                    where.add("("+ mask._Fld52995 +" >= "+ priceFrom +")");
                }
                if(priceTo != 0){
                    where.add("("+ mask._Fld52995 +" <= "+ priceTo +")");
                }
            }
            
            if( isEat() ){
                where.add("(T1."+ mask.newEatField +" in("+ getEat() +"))");
            }
            
            if( hotels != null ){
                where.add("(T1."+ mask._Fld52988RRef_1 +" in("+ getHotels() +"))");
            }
            
//            full.put("o", where.makeing());
            where.add("T1._Fld60937 = 0");
            Sql.parse("priceInValutaSting", "(CAST((T1."+ mask._Fld52995 +" * 1.0) AS NUMERIC(17, 8)) / 1.0)");
            Sql.parse("TOP", "30");
            Sql.parse("PUSTO", "0x00000000000000000000000000000000");
            Sql.parse("_Fld52990", mask._Fld52990);
            Sql.parse("_Fld52989", mask._Fld52989);
            Sql.parse("_Fld52988RRef", mask._Fld52988RRef);
            Sql.parse("_Fld52995", mask._Fld52995);
            Sql.parse("_Fld52991RRef", mask._Fld52991RRef);
            Sql.parse("_Fld52994RRef", mask._Fld52994RRef);
            Sql.parse("_Fld52988RRef_1", mask._Fld52988RRef_1);
            Sql.parse("_Fld52993RRef", mask._Fld52993RRef);
            Sql.parse("_Fld52992RRef", mask._Fld52992RRef);
            Sql.parse("table_name", mask.table_name);
            Sql.parse("WHERE", where.makeing());
            
            log("######################################################\n");
            log(Sql.get());
            
            if(!Sql.isEmpty()){
                try {
                    int n = 0;
                    int priceCoef = 0;
                    rs = db.executeQuery(Sql.get());
                    while (rs.next()) {
                        n++;
                        priceCoef = 0;
                        Map<String, String> m = new HashMap<>();
                        m.put("number", Integer.toString(n));
                        m.put("DatesStr", rs.getString("datefrom") + "/" + rs.getString("dateto"));
                        m.put("nights", rs.getString("Q_001_F_003_"));
                        m.put("hotel", rs.getString("d3") + ", " + rs.getString("d14"));
                        m.put("resort", rs.getString("d9"));
                        m.put("meal", rs.getString("d5"));
                        m.put("nomer", rs.getString("d4"));
                        m.put("spo", rs.getString("d1"));
                        m.put("curr", Helper.getCur(rs.getString("d7").toLowerCase()));
                        m.put("price", rs.getString("Q_001_F_008_"));
                        priceCoef = Integer.parseInt(rs.getString("Q_001_F_008_")) + this.rt.getCoef(rs.getString("datefrom"), rs.getString("spoId"));
                        m.put("avia_e_to", rs.getString("aviaeconom"));
                        m.put("avia_e_from", rs.getString("aviaeconomback"));
                        m.put("avia_stop_to", this.getStopSale(rs.getString("stopavia")));
                        m.put("avia_stop_from", this.getStopSale(rs.getString("stopaviato")));
                        m.put("hotel_places", rs.getString("hotelfree"));
                        m.put("hotel_stop", this.getStopSale(rs.getString("hotelstop")));
                        
                        //hotelstop
                        //m.put("priceFmt", rs.getString("Q_001_F_008_") + Helper.getCur(rs.getString("d7").toLowerCase()));
                        m.put("priceCoef", Integer.toString(priceCoef));
                        if( n % 2 == 0 ){
                            m.put("od", "true");
                        } else {
                            m.put("od", "false");
                        }
                        list.put(n, m);
                    }
                    full.put("list", list);
                } catch (Exception e) {
                    full.put("error", "Error 002012: " + e.getMessage());
                }
            } else {
                full.put("error", "Error 002011");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Find.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        out.print(full.toJSONString());
    }
    
    private String getStopSale(Object value ) {
        if( value != null && value instanceof String && value.equals("01") ) {
            return "1";
        } else {
            return "0";
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
