/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chief_Desktop
 */
public class GetForm extends HttpServlet {
    
    private MsSql db;
    
    private String type = "";
    private String country = "";
    private Integer wAvia = 1;
    private String city = "";
    private String spo = "";
    private String ttour = "";
    private String dateFrom = "";
    private String dateTo = "";
    private String adult = "";
    private String child = "";
    private String nightsFrom = "";
    private String nightsTo = "";
    private String earlyBron = "";
    private String mRo = "";
    private String mBb = "";
    private String mHb = "";
    private String mHbplus = "";
    private String mFb = "";
    private String mFbplus = "";
    private String mAi = "";
    private String mUai = "";
    private String hSecond = "";
    private String hThird = "";
    private String hFourth = "";
    private String hFifth = "";
    private String hApt = "";
    private String hVilla = "";
    private String priceFrom = "";
    private String priceTo = "";
    private String currency = "";
    private JSONObject full = new JSONObject();
    private ResultSet rs = null;
    private Template Sql = null;
    private String CTCVID = "";
    
    private String sqlDateFrom = "";
    private String sqlDateTo = "";
    private String sqlDateFromW = "";
    private String sqlDateToW = "";
    
    private static  String[] english = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q",
                                    "r","s","t","u","v","w","x","y","z",};     
    
    private static String [] russian = {"а","б","к","д","э","ф","г","х","и","ж","к","л","м","н","о","п","к",
                                        "р","с","т","у","в","в","кс","й","з"};

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        db = new MsSql();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp); //To change body of generated methods, choose Tools | Templates.
        try {
            if( !db.getStatus() ){
                db.renewStatus();
                Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "Connection closed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetCountry.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
        db.destroy();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GetForm</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GetForm at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    protected void getWavia() throws IOException{
        Sql = new Template(getServletConfig(), "query/wAvia.sql");
        Sql.parse("country", Helper.getBinar(country));
        
        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                while (rs.next()) {
                    wAvia = Integer.parseInt(rs.getString("_Fld54958"));
                }
            } catch (Exception e) {
//                out.println(e.getMessage());
            }
        }
        
    }
    protected void getCityList() throws IOException {
        if( wAvia == 1 ){
            Sql = new Template(getServletConfig(), "query/CitiesWAvia.sql");
            Sql.parse("country", Helper.getBinar(country));
        } else {
            Sql = new Template(getServletConfig(), "query/CitiesWoAvia.sql");
        }

        JSONObject cities = new JSONObject();

        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                int n = 0;
                // Iterate through the data in the result set and display it.
                while (rs.next()) {
                    n++;
                    Map<String, String> m = new HashMap<>();
                    m.put("id", rs.getString("idx"));
                    m.put("title", rs.getString("_Description"));
                    if( m.get("id").equals("93FA60A44CCD569311E312F283983A09") ){
                        m.put("df", "true");
                        if( city.isEmpty() ){
                            city = m.get("id");
                        }
                    }
                    cities.put(n, m);
                }
                full.put("depCities", cities);
            } catch (SQLException e){
//                out.println("SQLException: " + e.toString() + "\nQuery: " + Sql.get());
            }
            catch (Exception e) {
//                out.println("Exception: " + e.toString());
            }
        }
    }
    
    protected void getCTCVID() throws IOException {
        if( wAvia == 1 ){
            Sql = new Template(getServletConfig(), "query/getCTCVID_New.sql");
            Sql.parse("country", Helper.getBinar(country));
            Sql.parse("city", Helper.getBinar(city));
        } else {
            Sql = new Template(getServletConfig(), "query/getCTCVID.sql");
            Sql.parse("country", Helper.getBinar(country));
            Sql.parse("city", Helper.getBinar(city));
        }
        
        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                List<String> strings = new LinkedList<>();
                while (rs.next()) {
                    strings.add(Helper.getBinar(rs.getString("_IDRRef")));
                }
                CTCVID = String.join(",", strings);
//                out.println(CTCVID);
                
            } catch (Exception e) {
//                out.println(e.getMessage());
            }
        }
    }
    
    protected void getParameters() throws IOException {
        
        Sql = new Template(getServletConfig(), "query/getParameters.sql");
        Sql.parse("country", Helper.getBinar(country));
        
        if(!Sql.isEmpty()){
            try {
                Date nowDate = new Date();
                SimpleDateFormat frontFmt = new SimpleDateFormat("dd.MM.yyyy");
                SimpleDateFormat sqlFmt = new SimpleDateFormat("yyyyMMdd");
                JSONObject parameters = new JSONObject();
                rs = db.executeQuery(Sql.get());
                parameters.put("defaultFrom", "7");
                parameters.put("defaultTo", "14");
                parameters.put("defaultPeriod", "14");
                
                parameters.put("defaultDateFromFmt", Helper.getDate(nowDate, frontFmt));
                parameters.put("defaultDateToFmt", Helper.getDate(nowDate, 14, "day", frontFmt));
                
                sqlDateFrom = Helper.getDate(nowDate, sqlFmt);
                sqlDateTo = Helper.getDate(nowDate, 14, "day", sqlFmt);
                sqlDateFromW = Helper.getWYDate(nowDate, sqlFmt);
                sqlDateToW = Helper.getWYDate(nowDate, 14, "day", sqlFmt);
                
                parameters.put("wAvia", wAvia.toString());
                long CurEpo = new Date().getTime() / 1000;
                while (rs.next()) {
                    long b = 1000;
                    long TimeEpo = rs.getLong("stampDateFrom");
                    parameters.replace("defaultFrom", rs.getString("_Fld54284"));
                    parameters.replace("defaultTo", rs.getString("_Fld54285"));
                    parameters.replace("defaultPeriod", rs.getString("_Fld54286"));
                    parameters.replace("wAvia", Integer.parseInt(rs.getString("_Fld54958")));
                    if(TimeEpo > CurEpo){
                        nightsFrom = rs.getString("_Fld54284");
                        nightsTo = rs.getString("_Fld54285");
                        nowDate.setTime(TimeEpo * b);
                        parameters.replace("defaultDateFromFmt", Helper.getDate(nowDate, frontFmt));
                        parameters.replace("defaultDateToFmt", Helper.getDate(nowDate, rs.getInt("_Fld54286"), "day", frontFmt));
                        sqlDateFrom = Helper.getDate(nowDate, sqlFmt);
                        sqlDateFromW = Helper.getWYDate(nowDate, sqlFmt);
                        sqlDateTo = Helper.getDate(nowDate, rs.getInt("_Fld54286"), "day", sqlFmt);
                        sqlDateToW = Helper.getWYDate(nowDate, rs.getInt("_Fld54286"), "day", sqlFmt);
                    }
                }
                full.put("parameters", parameters);
            } catch (Exception e) {
//                out.println(e.getMessage());
            }
        }
    }
    
    protected void getSpoList() throws IOException {
        
        Sql = new Template(getServletConfig(), "query/getSpo.sql");
        if( wAvia == 1 ){
            Sql.parse("WOavia", "AND T1._Fld54973 = 0x01");
        } else {
            Sql.parse("WOavia", "AND T1._Fld54974 = 0x01");
        }
        Sql.parse("country", Helper.getBinar(country));
        Sql.parse("city", Helper.getBinar(city));

        JSONObject spoList = new JSONObject();
            
        if(!Sql.isEmpty()){
            int n = 0;
            Map<String, String> m = new HashMap<>();
            m.put("id", "0");
            m.put("title", "Все");
            m.put("df", "true");
            spoList.put(n, m);
            try {
                rs = db.executeQuery(Sql.get());
                while (rs.next()) {
                    n++;
                    m = new HashMap<>();
                    m.put("id", rs.getString("_IDRRef"));
                    m.put("title", rs.getString("_Description"));
                    spoList.put(n, m);
                }
                full.put("spoList", spoList);
            } catch (SQLException e){
//                out.println("SQLException: " + e.toString() + "\nQuery: " + Sql.get());
            } catch (Exception e) {
//                out.println("Exception: " + e.toString());
            }
        }
    }
    
    protected void getNights() throws IOException {
        Sql = new Template(getServletConfig(), "query/getParametersBySpo.sql");
        
        Fltr filter = new Fltr();
        
        filter.add("(T1._Fld54346RRef in ("+ CTCVID +"))");
        filter.add("((T1._Fld54347 >= '"+ sqlDateFromW +"') AND (T1._Fld54347 <= '"+ sqlDateToW +"'))");
        Sql.parse("where", filter.make());
        
        JSONObject nightsList = new JSONObject();
        
        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                int n = 0;
                // Iterate through the data in the result set and display it.
                while (rs.next()) {
                    n++;
                    nightsList.put(n, rs.getString("num"));
                }
                full.put("nightsList", nightsList);
            } catch (SQLException e){
                log("SQLException: " + e.toString() + "\nQuery: " + Sql.get());
            }
            catch (Exception e) {
                log("Exception: " + e.toString());
            }
        }
    }
    
    protected void getTourTypes() throws IOException {
        Sql = new Template(getServletConfig(), "query/getTourType.sql");
        Sql.parse("ctcvid", CTCVID);
        Sql.parse("dateFrom", sqlDateFromW);
        Sql.parse("dateTo", sqlDateToW);
        Sql.parse("nightsFrom", nightsFrom);
        Sql.parse("nightsTo", nightsTo);
        
        JSONObject tourTypes = new JSONObject();

        if(!Sql.isEmpty()){
            int n = 1;
            
            Map<String, String> m = new HashMap<>();
            String id = "";
            String title = "";
            m.put("id", "0");
            m.put("title", "Все");
            m.put("df", "true");
            tourTypes.put(n, m);
            
            Map<String, Boolean> existTypes = new HashMap<>();
            
                log(Sql.get());
            try {
                rs = db.executeQuery(Sql.get());
                while (rs.next()) {
                    if(!rs.getString("_Fld49580RRef").equals("00000000000000000000000000000000")){
                        if( existTypes.get(rs.getString("_Fld49580RRef")) == null ){
                            existTypes.put(rs.getString("_Fld49580RRef"), Boolean.TRUE);
                            m = new HashMap<>();
                            n++;
                            m.put("id", rs.getString("_Fld49580RRef"));
                            m.put("title", rs.getString("Name_Fld49580RRef"));
                            tourTypes.put(n, m);
                        }
                    }
                    if(!rs.getString("_Fld53047RRef").equals("00000000000000000000000000000000")){
                        if( existTypes.get(rs.getString("_Fld53047RRef")) == null ){
                            existTypes.put(rs.getString("_Fld53047RRef"), Boolean.TRUE);
                            m = new HashMap<>();
                            n++;
                            m.put("id", rs.getString("_Fld53047RRef"));
                            m.put("title", rs.getString("Name_Fld53047RRef"));
                            tourTypes.put(n, m);
                        }
                    }
                    if(!rs.getString("_Fld53048RRef").equals("00000000000000000000000000000000")){
                        if( existTypes.get(rs.getString("_Fld53048RRef")) == null ){
                            existTypes.put(rs.getString("_Fld53048RRef"), Boolean.TRUE);
                            m = new HashMap<>();
                            n++;
                            m.put("id", rs.getString("_Fld53048RRef"));
                            m.put("title", rs.getString("Name_Fld53048RRef"));
                            tourTypes.put(n, m);
                        }
                    }
                    if(!rs.getString("_Fld53049RRef").equals("00000000000000000000000000000000")){
                        if( existTypes.get(rs.getString("_Fld53049RRef")) == null ){
                            existTypes.put(rs.getString("_Fld53049RRef"), Boolean.TRUE);
                            m = new HashMap<>();
                            n++;
                            m.put("id", rs.getString("_Fld53049RRef"));
                            m.put("title", rs.getString("Name_Fld53049RRef"));
                            tourTypes.put(n, m);
                        }
                    }
                    if(!rs.getString("_Fld53081RRef").equals("00000000000000000000000000000000")){
                        if( existTypes.get(rs.getString("_Fld53081RRef")) == null ){
                            existTypes.put(rs.getString("_Fld53081RRef"), Boolean.TRUE);
                            m = new HashMap<>();
                            n++;
                            m.put("id", rs.getString("_Fld53081RRef"));
                            m.put("title", rs.getString("Name_Fld53081RRef"));
                            tourTypes.put(n, m);
                        }
                    }
                }
                full.put("TourTypesList", tourTypes);
                
            } catch (SQLException e){
                log("SQLException:\n " + e.getMessage());
            } catch (Exception e) {
                log("Exception:\n " + e.getMessage());
            }
        }
    }
    
    protected void getFlyData() throws IOException {
        Sql = new Template(getServletConfig(), "query/getFlyDate.sql");
        Sql.parse("P8", Helper.getBinar(city));
        Sql.parse("P9", "(T6._OwnerIDRRef = " + Helper.getBinar(country) + ") AND ");
        
        SimpleDateFormat sqlFmt = new SimpleDateFormat("yyyyMMdd");
        Date nowDate = new Date();
        String nowDateW = Helper.getWYDate(nowDate, sqlFmt);
        Sql.parse("P10", nowDateW);
        Sql.parse("P11", (wAvia == 1)?"0xB3E20EE28C8D1498400D80C9C5491574":"0x860C6933C4DFFAA64723F936449647FC");
        Sql.parse("cityin", "");
        Sql.parse("transporter", "");
        
        JSONObject flyDates = new JSONObject();
        JSONObject normal = new JSONObject();
        JSONObject yellow = new JSONObject();
        JSONObject stopsale = new JSONObject();
        flyDates.put("normal", normal);
        flyDates.put("yellow", yellow);
        flyDates.put("stopsale", stopsale);
        if(!Sql.isEmpty()){
            int n = 0;
            try {
                rs = db.executeQuery(Sql.get());
                while (rs.next()) {
                    n++;
                    String toDate = rs.getString("fromDay") + "." + rs.getString("fromMonth") + "." + rs.getString("fromYear");
                    int isStopsale = Integer.parseInt(rs.getString("stopsale"));
                    int mesta = Integer.parseInt(rs.getString("mesta"));
                    if(isStopsale == 1 && mesta == 0){
                        stopsale.put(n, toDate);
                    }
                    if(isStopsale == 0 && mesta == 0){
                        yellow.put(n, toDate);
                    }
                    if(mesta == 1){
                        normal.put(n, toDate); 
                    }
                }
            } catch (SQLException e){
                log("SQLException: " + e.getMessage()+ "\nQuery: " + Sql.get());
            }
            catch (Exception e) {
                log("Exception: " + e.toString());
            }
        }
        
        full.put("FlyDate", flyDates);
    }
    
    protected void getCityResort() throws IOException {
        Sql = new Template(getServletConfig(), "query/getCityHotels.sql");
        Sql.parse("realIdFltr", "(T2._Fld53305RRef = 0x00000000000000000000000000000000) OR (T2._Fld53305RRef = 0x00000000000000000000000000000000)");
        Sql.parse("owner2Fltr", "((T2._Fld53304RRef = 0x00000000000000000000000000000000) OR (T2._Fld53304RRef = 0x00000000000000000000000000000000))");
        
        Fltr where = new Fltr();
        Fltr subwhere = new Fltr();
        
        where.add("(T1._Fld52638RRef = " + Helper.getBinar(country) + ")");
        where.add("(T2._Fld52411 >= '"+ sqlDateFromW +"') AND (T2._Fld52411 <= '"+ sqlDateToW +"')");
        where.add("T5._Description is not null and T4._Description is not null");
        subwhere.add("((T10._Fld53307RRef = " + Helper.getBinar(country) + ") OR (T10._Fld53307RRef = 0x00000000000000000000000000000000))");
        
        SimpleDateFormat sqlFmt = new SimpleDateFormat("yyyyMMdd");
        Date nowDate = new Date();
        String nowDateW = Helper.getWYDate(nowDate, sqlFmt);
        Sql.parse("nowDate", nowDateW);
        Sql.parse("WHERE", where.make());
        Sql.parse("SUBWHERE", subwhere.make());
        Sql.parse("t11_1", "");
        Sql.parse("t11_2", "");
        Sql.parse("t11_3", "");
        
        String hotelTitle = "";
        String hotelTitleSrch = "";
        String resortTitle = "";
        String cityTitle = "";
        
        LinkedHashMap<String, Object> hotels = new LinkedHashMap<>();
        LinkedHashMap<String, Object> cities = new LinkedHashMap<>();
        LinkedHashMap<String, Object> citiesSorted = new LinkedHashMap<>();
        LinkedHashMap<String, Object> resorts = new LinkedHashMap<>();
        LinkedHashMap<String, Object> resortsSorted = new LinkedHashMap<>();
        
        if(!Sql.isEmpty()){
                log("##################################################################\n" + Sql.get());
            try {
                int n = 0;
                rs = db.executeQuery(Sql.get());
                while (rs.next()) {
                    
                    hotelTitle = rs.getString("_Description").toLowerCase();
                    hotelTitleSrch = hotelTitle.replaceAll("[^a-z\\p{Blank}]", "");
                    hotelTitle = hotelTitle.replaceAll("[^a-z]", "");
                    
                    cityTitle = rs.getString("cityName").toLowerCase();
                    cityTitle = cityTitle.replaceAll("[^a-zа-я]", "");
                    
                    
                    if(cities.get(cityTitle) == null){
                        Map<String, String> cityMap = new HashMap<>();
                        
                        cityMap.put("id", rs.getString("cityId"));
                        cityMap.put("title", rs.getString("cityName"));
                        cityMap.put("search", rs.getString("cityName").toLowerCase());
                        cities.put(cityTitle, cityMap);
                    }
                    
                    resortTitle = rs.getString("resortName").toLowerCase();
                    resortTitle = resortTitle.replaceAll("[^a-zа-я]", "");
                    if( resorts.get(resortTitle) == null ){
                        Map<String, String> resortMap = new HashMap<>();
                        
                        resortMap.put("id", rs.getString("resortId"));
                        resortMap.put("title", rs.getString("resortName"));
                        resortMap.put("cityId", rs.getString("cityId"));
                        resortMap.put("search", rs.getString("resortName").toLowerCase());

                        resorts.put(resortTitle, resortMap);
                    }
                    
                    if(hotels.get(hotelTitle) == null){
                        Map<String, String> hotelMap = new HashMap<>();
                        hotelMap.put("id", rs.getString("id"));
                        hotelMap.put("nid", rs.getString("newId"));
                        hotelMap.put("title", rs.getString("_Description"));
                        hotelMap.put("cityId", rs.getString("cityId"));
                        hotelMap.put("resortId", rs.getString("resortId"));
                        hotelMap.put("stars", rs.getString("star_num"));
                        hotelMap.put("search", hotelTitleSrch);
                        
                        hotels.put(hotelTitle, hotelMap);
                    }
                }
                cities.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(entry -> {
                    citiesSorted.put(entry.getKey(), entry.getValue());
                });
                resorts.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(entry -> {
                    resortsSorted.put(entry.getKey(), entry.getValue());
                });
            } catch (Exception e) {
                Logger.getLogger(GetForm.class.getName()).log(Level.WARNING, e.getMessage());
                log("##################################################################\n" + Sql.get());
            }
            full.put("citiesList", citiesSorted);
            full.put("hotelsList", hotels);
            full.put("resortsList", resortsSorted);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        type = request.getParameter("type");
        country = request.getParameter("country");
        wAvia = Integer.parseInt(request.getParameter("wAvia"));
        city = request.getParameter("city");
        spo = request.getParameter("spo");
        ttour = request.getParameter("ttour");
        dateFrom = request.getParameter("dateFrom");
        dateTo = request.getParameter("dateTo");
        adult = request.getParameter("adult");
        child = request.getParameter("child");
        nightsFrom = request.getParameter("nightsFrom");
        nightsTo = request.getParameter("nightsTo");
        earlyBron = request.getParameter("earlyBron");
        mRo = request.getParameter("mRo");
        mBb = request.getParameter("mBb");
        mHb = request.getParameter("mHb");
        mHbplus = request.getParameter("mHbplus");
        mFb = request.getParameter("mFb");
        mFbplus = request.getParameter("mFbplus");
        mAi = request.getParameter("mAi");
        mUai = request.getParameter("mUai");
        hSecond = request.getParameter("hSecond");
        hThird = request.getParameter("hThird");
        hFourth = request.getParameter("hFourth");
        hFifth = request.getParameter("hFifth");
        hApt = request.getParameter("hApt");
        hVilla = request.getParameter("hVilla");
        priceFrom = request.getParameter("priceFrom");
        priceTo = request.getParameter("priceTo");
        currency = request.getParameter("currency");
        PrintWriter out = response.getWriter();
        
        long timeout = 0;
        boolean isLog = false;
        
        full.clear();
        
        if( type.equals("renewCntr") ){
            timeout = System.currentTimeMillis();
            getWavia(); 
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getWavia: {0}", timeout);
        }
        
        //# get city
        if( type.equals("renewCntr") || type.equals("renewWavia") ){
            timeout = System.currentTimeMillis();
            getCityList();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getCityList: {0}", timeout);
        }
        //#get CTCVID
            timeout = System.currentTimeMillis();
        getCTCVID();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getCTCVID: {0}", timeout);
        
        //# get parameters
        if( type.equals("renewCntr") ){
            timeout = System.currentTimeMillis();
        getParameters();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getParameters: {0}", timeout);
        }
        
        //# get list of spo
        if( type.equals("renewCntr") || type.equals("renewCity") || type.equals("renewWavia") ){
            timeout = System.currentTimeMillis();
            getSpoList();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getSpoList: {0}", timeout);
        }
        
        // get list of nights
        if( type.equals("renewCntr") || type.equals("renewCity") || type.equals("renewWavia") ){
            timeout = System.currentTimeMillis();
            getNights();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getNights: {0}", timeout);
        }
        
        // get tour types
        if( type.equals("renewCntr") || type.equals("renewCity") || type.equals("renewWavia") ){
            timeout = System.currentTimeMillis();
            getTourTypes();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getTourTypes: {0}", timeout);
        }
        if( type.equals("renewCntr") || type.equals("renewWavia") ){
            timeout = System.currentTimeMillis();
            getFlyData();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getFlyData: {0}", timeout);
        }
        if( type.equals("renewCntr") ){
            timeout = System.currentTimeMillis();
            getCityResort();
            timeout = System.currentTimeMillis() - timeout;
            if(isLog) Logger.getLogger(GetForm.class.getName()).log(Level.SEVERE, "getCityResort: {0}", timeout);
        }
        
        out.print(full.toJSONString());
        
//        processRequest(request, response);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
