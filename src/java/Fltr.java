
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Fltr extends ArrayList {
        
    public void erase() {
        this.clear();
    }

    public void add(String param){
        super.add(param);
    }
    
    protected String rMake(String separator){
        String result = "";
        
        result = this.toString();
        result = result.replaceAll(", ", " "+ separator +" ");
        result = result.replaceAll("\\[|\\]", "");
        
        return result;
    }
    
    public String makeing(){
        String result = "";
        int n = 0;
        for (Object item: this) {
            n++;
            if(n == 1){
                result += item;
            } else {
                result += " and " + item;
            }
        }
        
        return result;
    }

    public String make() {
        String result;

        result = rMake("and");
        
        return result;
    }
    
    public String make(String sep){
        String result;

        result = rMake(sep);

        return result;
    }
}
