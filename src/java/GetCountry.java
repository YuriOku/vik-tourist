/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Chief_Desktop
 */
public class GetCountry extends HttpServlet {
    
    private MsSql db;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        db = new MsSql();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp); //To change body of generated methods, choose Tools | Templates.
        try {
            if( !db.getStatus() ){
                db.renewStatus();
                Logger.getLogger(GetCountry.class.getName()).log(Level.SEVERE, "Connection closed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetCountry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
        db.destroy();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        ResultSet rs = null;
        Template Sql = new Template(getServletConfig(), "query/Countries.sql");
        JSONObject full = new JSONObject();
        
        if(!Sql.isEmpty()){
            try {
                rs = db.executeQuery(Sql.get());
                int n = 0;
                // Iterate through the data in the result set and display it.
                while (rs.next()) {
                    n++;
                    Map<String, String> m = new HashMap<>();
                    m.put("id", rs.getString("_IDRRef"));
                    m.put("title", rs.getString("_Description"));
                    String o = m.get("id");
                    if( m.get("id").equals("93FA60A44CCD569311E31341E67F1E93") ){
                        m.put("df", "true");
                    }
                    full.put(n, m);
                }
                out.print(full.toJSONString());
            } catch (SQLException e){
                out.println("SQLException: " + e.toString() + "\nQuery: " + Sql.get());
            }
            catch (Exception e) {
                out.println("Exception: " + e.toString());
            }
            finally {
                if (rs != null) try { rs.close(); } catch(Exception e) {}
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
