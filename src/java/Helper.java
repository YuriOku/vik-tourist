
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class Helper {
    
    private static Map<String, String> cur = new HashMap<>();
    
    public static void setCur(String key, String value){
        cur.put(key, value);
    }
    public static String getCur(String key){
        return cur.get(key);
    }
    
    public static String getBinar(String value) {
        return "0x" + value;
    }
    
    public static String getDate(Date d, SimpleDateFormat f) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        Date ndt = c.getTime();
        return f.format(ndt);
    }
    
    public static String getDate(Date d,Integer count,String type,SimpleDateFormat f){
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        switch (type) {
            case "day" : {
                c.add(Calendar.DAY_OF_YEAR, count);break;
            }
            case "month" : {
                c.add(Calendar.MONTH, count);break;
            }
            case "year" : {
                c.add(Calendar.YEAR, count);break;
            }
        }
        Date ndt = c.getTime();
        return f.format(ndt);
    }
    
    public static String getWYDate(Date d, SimpleDateFormat f) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.YEAR, 2000);
        Date ndt = c.getTime();
        return f.format(ndt);
    }
    public static String getWYDate(Date d,Integer count,String type,SimpleDateFormat f){
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.YEAR, 2000);
        switch (type) {
            case "day" : {
                c.add(Calendar.DAY_OF_YEAR, count);break;
            }
            case "month" : {
                c.add(Calendar.MONTH, count);break;
            }
            case "year" : {
                c.add(Calendar.YEAR, count);break;
            }
        }
        Date ndt = c.getTime();
        return f.format(ndt);
    }
    
    public static String getDate(long m,SimpleDateFormat f) {
        
        Date df = new Date(m);
        
        return f.format(df);
    }
}
