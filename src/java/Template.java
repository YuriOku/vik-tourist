
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletConfig;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class Template {
    private static String tpl = "";
    private final ServletConfig config;
    private String fullPath;
    private File queryFile;
    private String message;

    public Template(ServletConfig config, String fileLocation) throws IOException {
        this.config = config;
        tpl = "";
        RegisterFile(fileLocation);
        if( CheckFile() ){
            RegisterTemplate();
        }
    }
    
    
    public String get() {
        if(isEmpty()){
            return getMessage();
        } else {
            return tpl;
        }
    }
    
    public boolean isEmpty() {
        if( tpl.isEmpty() ){
            return true;
        } else {
            return false;
        }
    }
    
    public void parse(String key,String value) {
        tpl = tpl.replaceAll("\\{" + key + "\\}", value);
    }
    
    private void RegisterFile(String fileLocation) {
        String path = config.getServletContext().getRealPath("/");
        fullPath = path + fileLocation;
        queryFile = new File(fullPath);
    }
    
    public String getMessage(){
        return message;
    }
    
    private boolean CheckFile() {
        if( queryFile.exists() && !queryFile.isDirectory() ){
            return true;
        } else {
            if(!queryFile.exists()) {
                message = "File not exist" + queryFile.getPath();
            }
            if(queryFile.isDirectory()){
                message = "File is dir";
            }
            return false;
        }
    }
    
    public void RegisterTemplate() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(
                new FileInputStream(fullPath),
                "UTF8"
            )
        );

        String temp;
        while ((temp = bufferedReader.readLine()) != null) {
            tpl += "\n" + temp;
        }
    }
}
