
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chief_Desktop
 */
public class MySql {
    // JDBC driver name and database URL
    final String JDBC_DRIVER="com.mysql.jdbc.Driver";
    final String DB_URL="jdbc:mysql://pinco.mysql.ukraine.com.ua/pinco_paravozov";
    //  Database credentials
    final String USER = "pinco_paravozov";
    final String PASS = "gh27kbll";
    // Declare the JDBC objects.
    Connection con = null;
    Statement stmt = null;
    
    public MySql() {
        try {
            // Establish the connection.
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            // Create and execute an SQL statement that returns some data.
            stmt = con.createStatement();
            Logger.getLogger(MySql.class.getName()).log(Level.SEVERE, "Open connection and get statement");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(MySql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ResultSet executeQuery(String sql) throws SQLException {
        return stmt.executeQuery(sql);
    }
    
    public void destroy() {
        if (stmt != null) try { 
            stmt.close();
        } catch(Exception e) {}
        if (con != null) try {
            Logger.getLogger(MySql.class.getName()).log(Level.SEVERE, "Close connection and statement");
            con.close();
        } catch(Exception e) {}
    }
}
