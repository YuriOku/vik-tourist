
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yurchik
 */
public class Ratio {
    
    private static Ratio instance;
    
    private Map<String, String> m = new HashMap<>();
    
    private int columnNumber = 0;

    static {
        instance = new Ratio();
        // В этом блоке возможна обработка исключений
    }

    private Ratio () {}

    public static Ratio getInstance() {
        return instance;
    }
    
    public int getColumnNumber() {
        return columnNumber;
    }
    
    public void showFirst() {
        int n = 0;
        for( Map.Entry<String, String> entry : m.entrySet() ){
            Logger.getLogger("showFirst").log(Level.INFO, entry.getKey());
        }
        Logger.getLogger("showFirst").log(Level.INFO, "END : " + m.get("30.10.16_81CA002590FD6FDB11E57EFEED0AF2BD"));
    }
    
    public int getCoef(String date, String spo) {
        int coef = 0;
        if( this.hasCoef(this.getKeyName(date, spo)) ) {
            coef = Integer.parseInt(this.m.get( this.getKeyName(date, spo) ));
        }
        return coef;
    }
    
    private boolean hasCoef(String key) {
        return this.m.containsKey(key);
    }
    
    private String getKeyName(String date, String spo) {
        return date + "_" + spo;
    }
    
    public void setColumnNumber(ResultSet coefRange, int nightFrom) throws SQLException {
        while (coefRange.next()) {
            if(nightFrom <= coefRange.getInt("Fld57577_")){
                columnNumber = 10;
            }
            if(nightFrom <= coefRange.getInt("Fld57576_")){
                columnNumber = 9;
            }
            if(nightFrom <= coefRange.getInt("Fld57575_")){
                columnNumber = 8;
            }
            if(nightFrom <= coefRange.getInt("Fld57574_")){
                columnNumber = 7;
            }
            if(nightFrom <= coefRange.getInt("Fld57573_")){
                columnNumber = 6;
            }
            if(nightFrom <= coefRange.getInt("Fld57572_")){
                columnNumber = 5;
            }
            if(nightFrom <= coefRange.getInt("Fld57571_")){
                columnNumber = 4;
            }
            if(nightFrom <= coefRange.getInt("Fld57570_")){
                columnNumber = 3;
            }
            if(nightFrom <= coefRange.getInt("Fld57569_")){
                columnNumber = 2;
            }
            if(nightFrom <= coefRange.getInt("Fld57568_")){
                columnNumber = 1;
            }
        }
    }
    
    public void setCoefItem(ResultSet coefItems) throws SQLException {
        this.m.clear();
        while (coefItems.next()) {
            this.m.put(this.getKeyName(coefItems.getString("date"), coefItems.getString("spo")) , coefItems.getString("kPerPerson_" + columnNumber));
        }
    }
}
