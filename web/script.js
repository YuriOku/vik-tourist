var _location = location.origin;
var calendar = calendar || {};
(function (calendar) {
    calendar.picker = function (dates,tooltips,target) {
        dates = JSON.parse(JSON.stringify(dates));
        var n = 0;
        tooltips = tooltips || {};
        $(target).datepicker("destroy");
        $(target).datepicker({
            numberOfMonths: 3,
            minDate: -6,
            dateFormat: 'dd.mm.yy',
            beforeShowDay: function(date) {
               var m = date.getMonth(),d = date.getDate(),y = date.getFullYear();
               if( typeof(dates['green']) == 'object' ){
                   for(var key in dates['green']){
                       if( d+'.'+(m + 1)+'.'+y == dates['green'][key] ){
                           return [true, 'highlight_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                       }
                   }
               }
               if( typeof(dates['yellow']) == 'object' ){
                    for(var key in dates['yellow']){
                        if( d+'.'+(m + 1)+'.'+y == dates['yellow'][key] ){
                            return [true, 'yellow_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                        }
                    }
               }
               if( typeof(dates['stop']) == 'object' ){
                    for(var key in dates['stop']){
                        if( d+'.'+(m + 1)+'.'+y == dates['stop'][key] ){
                            return [true, 'stopsale_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                        }
                    }
               }
               return [true];
            },
            onSelect: function(date,inst) {
                var name = $('#' + inst.id).attr('data-name');
                if(name && typeof(name) == 'string'){
                    form[name] = date;
                }
            },
            onClose : function() {
                calendar.chechDate();
            }
	});
    };
    
    calendar.chechDate = function(date,type) {
        var dateFrom = $('[data-name="dateFrom"]').datepicker( "getDate" ),dateTo = $('[data-name="dateTo"]').datepicker( "getDate" );
        if( dateFrom > dateTo ){
            $( '[data-name="dateTo"]' ).datepicker( "setDate", dateFrom );
        }
    }
    
    calendar.setDate = function(date,target) {
        $(target).datepicker('setDate', date);
    };

} (calendar));

var hotelMask = { '2*' : 'hSecond','3*' : 'hThird','4*' : 'hFourth','5*' : 'hFifth','APT' :'hApt','VILLA' :'hVilla' };

var AviaSwitcher = AviaSwitcher || {};
(function (switcher) {
    switcher.o = $('[data-switcher="Avia"]');
    
    switcher.o.find('input').on('change',function() {
        if( $(this).prop('checked') ){
            form.wAvia = 1;
        } else {
            form.wAvia = 0;
        }
        RenewFrom('renewWavia');
    });
    
    switcher.set = function(i) {
        if(i == true){
            form.wAvia = 1;
            switcher.o.find('input').prop('checked', true);
        } else {
            form.wAvia = 0;
            switcher.o.find('input').prop('checked', false);
        }
    };
    
} ( AviaSwitcher ));

// SELECT
var $tour_form = $('.tour-container');

var tour_select = {
	select 		: $('.js-select'),
	select_t 	: '.js-select',
	value_t 	: '.tour-select__value',

	init:function(){
		// console.log(this.select);
	},
	listeners:function(){
		$tour_form.on('click', this.select_t, this.showList );
		$tour_form.on('click', this.value_t, this.selectValue );
        $tour_form.on('click','.tour-select__box',function(ev){
            ev.stopImmediatePropagation();
        });
	},
	showList: function(){	
		$(this).next('.tour-select__list').toggleClass('js-select-on');
	},
	selectValue: function(){
		var box = $(this).parents('.tour-select__box'),
			list = box.children('.tour-select__list'),
			select = box.children('.tour-select')

		list.removeClass('js-select-on');
		// Set value text
		select.text( $(this).text() );
	}
}// end tour_select

var form = {
    country:'',wAvia:0,city:'',spo:0,ttour:0,dateFrom:'',dateTo:'',adult:2,child:0,nightsFrom:7,nightsTo:14,earlyBron:false,
    mRo:false,mBb:false,mHb:false,mHbplus:false,mFb:false,mFbplus:false,mAi:false,mUai:false,
    hSecond:false,hThird:false,hFourth:false,hFifth:false,hApt:false,hVilla:false,
    priceFrom:0,priceTo:0,currency:'usd',stopSale:false,
    cities:{},hotels:{},resorts:[],type:''
};

var hotelList = new function() {
    this.par = $('[data-name="Hotels"]');
    this.search = $(this.par).find('[data-type="search"]');
    this.list = $(this.par).find('[data-hotels="list"]');
    this.keyCodes = [];
    this.oldText = '';
    
    this.fnsearch = function(text) {
        $(" > *",this.list).each(function() {
            var str = $(this).attr('data-search').toString(),rVisible = $(this).hasClass('r-disabled'),isVisible = !$(this).hasClass('disabled');
            if( str.search(text) != -1 ) {
                if( !rVisible ){
                    $(this).removeClass('disabled');
                }
            } else if(isVisible) {
                $(this).addClass('disabled');
            }
        });
        this.scrUpdate();
    };
    
    this.showAll = function() {
        $(this.list).children().removeClass('disabled');
        if( $(this.search).val() != ''){
            $(this.search).val('');
            this.oldText = '';
        }
        this.scrUpdate();
    };
    
    this.scrUpdate = function() {
        $(this.par).find('.tour-scrollbar').data("plugin_tinyscrollbar").update();
    };
    $(this.search).on("keydown",function() {
        hotelList.oldText = $(this).val();
    });
    $(this.search).on("keyup",function(event) {
        var text = $(this).val();
        if(text != '' && text != hotelList.oldText){
            if(event.keyCode != 17){
                hotelList.fnsearch(text);
            }
        } else if(text != hotelList.oldText) {
            hotelList.showAll();
        }
    });
};

var RegEx = {
    SearchText : new RegExp('([a-z]+[\\s]{0,1})','ig'),
    SearchCityText : new RegExp('([a-z]+[\\s]{0,1})','ig'),
    Number : new RegExp('([0-9])','ig'),
    UserInput : new RegExp('[a-z0-9_,-=<>\+\(\){}\|\\s\[\\]\^\$\|\?\.*~!@#%&`“”’"\'–]+', "ig"),
    woSymbols : new RegExp("[=<>\+{}\|\[\\]\^\$\|\?*~@]", "ig"),
    woSymbolsFirst : new RegExp("[«]", "ig"),
    
    filter : function(text,type) {
        if( text !== '' && typeof(this[type] !== "undefined")){
            var result = text.match(this[type]);
            var txt = result ? result.join('') : '';
            return txt;
        } else {
            return '';
        }
    },
    replace : function(text,type) {
        if( text !== '' && typeof(this[type]) !== "undefined"){
            var result = text.replace(this[type],'');
            if(result !== "undefined"){
                var txt = result;
                return txt;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
};

$(function(){
    
    var loader = $('[data-id="loader"]');
            $(loader).css({display:'block'});
    AviaSwitcher.set(true);
    $('.tour-scrollbar').tinyscrollbar({thumbSize : 30});
    getCountries();
    tour_select.listeners();

    var elem_body = $('body');
    elem_body.on('click',function(event){
        $('.tour-select__list').removeClass('js-select-on');
    });
    $('[name="checkbox-n1"], [name="checkbox-n2"]').change(function() {
        var gn = $(this).attr('data-group'),name = $(this).attr('name');
        $('[name="'+ name +'"]').each(function(){
            if( $(this).attr('data-group') != gn ){
                $(this).prop('checked', false);
            }
        });
    });
    
    $('[name="checkbox-stop_sale"]').change(function() {
        var status = $(this).prop('checked');
        if(status) {
            form.stopSale = 'true';
        } else {
            form.stopSale = 'false';
        }
    });
    
    $('[name="checkbox-early"]').change(function() {
        var status = $(this).prop('checked');
        if(status) {
            form.earlyBron = 'true';
        } else {
            form.earlyBron = 'false';
        }
    });
    
    $('[name="price-from"], [name="price-to"]').keyup(function(parameters) {
        var val = $(this).val(),nVal = RegEx.filter(val,'Number');
        if(nVal != val){
            $(this).val(nVal);
        }
        if( $(this).attr('name') == 'price-from' ){
            form.priceFrom = nVal;
        }
        if( $(this).attr('name') == 'price-to' ){
            form.priceTo = nVal;
        }
    });
    
    // bouble check
    $('.js-checkbox-double').change(function(){
            var status = $(this).prop('checked'),
                    checkbox = $(this).next(),
                    checkbox_list = $(this).parents('.tour-search-list__item')
                                            .find('.tour-search-list__in')
                                            .find('.tour-checkbox__input'),
                    marker = checkbox_list.next();

            checkbox_list.prop('checked', status);

            if(status){
                marker.addClass('js-checked');
                $(checkbox_list).each(function() {
                    var idx = $(this).attr('data-val');
                    resorts[idx]['selected'] = 1;
                });
            }else{
                $(checkbox_list).each(function() {
                    var idx = $(this).attr('data-val');
                    resorts[idx]['selected'] = 0;
                });
                marker.removeClass('js-checked');
                checkbox.removeClass('js-check-1');
            }
            hideHotels();
    });
    
    $('[data-chng="hotel"]').change(function() {
        var status = $(this).prop('checked'),
            parent = $(this).parents('[data-type="hotel"]'),
            id = $(parent).attr('data-val');
        
        if(status) {
            hotels[id]['selected'] = true;
        } else {
            hotels[id]['selected'] = false;
        }
    });

    $('.js-checkbox-list').change(function(){
        var status = $(this).prop('checked'),
            doubl = $(this).parents('.tour-search-list__item').find('.js-checkbox-double'),
            doubl_marker = doubl.next(),
            list = $(this).parents('.tour-search-list__in').find('.tour-checkbox__input'),
            length_list = list.length,
                parentId = $(this).parents('[data-branch="main"]').find('[data-box="active"]').attr('data-val'),
                thisId = $(this).attr('data-val'),
            length_check = list.filter(':checked').length;

            if(length_check == 0){
                doubl.prop('checked', false);
                doubl_marker.removeClass('js-check-1');
                resorts[thisId]['selected'] = 0;
            } else if(length_list == length_check){
                doubl.prop('checked', true);
                doubl_marker.removeClass('js-check-1');
                resorts[thisId]['selected'] = 1;
            } else if(length_list > length_check){
                doubl.prop('checked', false);
                doubl_marker.addClass('js-check-1');
                resorts[thisId]['selected'] = 1;
            }
            hideHotels();
    });
    $('.tour-checkbox__input').change(function(){
        var status = $(this).prop('checked'),
            marker = $(this).next();
        if(status){
            marker.addClass('js-checked')
        }else{
            marker.removeClass('js-checked')
        }
    });
    $('[name="checkbox-stars"]').each(function() {
        $(this).prop('checked',false);
    });
    $('[name="checkbox-stars"]').change(function() {
        var name = $(this).attr('data-name'),status = $(this).prop('checked');
        switch (name) {
            case 'hSecond':{ form.hSecond = status; break; }
            case 'hThird':{ form.hThird = status; break; }
            case 'hFourth':{ form.hFourth = status; break; }
            case 'hFifth':{ form.hFifth = status; break; }
            case 'hApt':{ form.hApt = status; break; }
            case 'hVilla':{ form.hVilla = status; break; }
        }
        checkHotelStars();
    });
    Starter.init();
});
var isStarSelected = false;
function checkHotelStars(isReturn) {
    var hArr = new Array();
    if(form.hSecond) hArr.push('hSecond');
    if(form.hThird) hArr.push('hThird');
    if(form.hFourth) hArr.push('hFourth');
    if(form.hFifth) hArr.push('hFifth');
    if(form.hApt) hArr.push('hApt');
    if(form.hVilla) hArr.push('hVilla');
    if(!isReturn){
        hideHotelsByStar(hArr);
        if( hArr.length > 0 ){
            isStarSelected = true;
        } else {
            isStarSelected = false;
        }
    } else {
        return hArr;
    }
    
}

function hideHotelsByStar(hArr) {
    var hList = $('[data-hotels="list"]');
    if(hArr.length > 0){
        $(hList).children().each(function() {
            var key = $(this).attr('data-stars'),hasClass = $(this).hasClass('h-disabled');
            if( $.inArray(key,hArr) == -1 && !hasClass){
                $(this).addClass('h-disabled');
            } else if( $.inArray(key,hArr) != -1 && hasClass ){
                $(this).removeClass('h-disabled');
            }
        });
    } else {
        $(hList).children().removeClass('h-disabled');
    }
    hotelList.scrUpdate();
}

function hideHotels() {
    var hList = $('[data-hotels="list"]'),rList = [];
    for(var key in resorts){
        if(resorts[key]['selected']){
            rList.push(key);
        }
    }
    
    if( rList.length > 0 ){
        $(hList).children().each(function() {
            var key = $(this).attr('data-resot'),hasClass = $(this).hasClass('r-disabled');
            if( $.inArray(key,rList) == -1 && !hasClass){
                $(this).addClass('r-disabled');
            } else if( hasClass ){
                $(this).removeClass('r-disabled');
            }
        });
    } else {
        $(hList).children().removeClass('r-disabled');
    }
    hotelList.showAll();
}

function selectCourse(obj,param) {
    var par = $(obj).parents('ul');
    $(par).find('li').removeClass('selected');
    $(obj).parent('li').addClass('selected');
    form.currency = param;
}

function getCountries() {
    
    var loader = $('[data-id="loader"]');
    $.ajax({
        type : 'post',
        url : location.origin + '/getCountry',
//        url : location.origin + '/get',
        dataType : 'json',
        beforeSend : function() {
            $(loader).css({display:'block'});
        },
        complete : function(response) {
//            $(loader).css({display:'none'});
        },
        success: function(response){
            if( response ){
                WorkWithCountries(response);
                RenewFrom('renewCntr',true);
            }
        },
        error: function(response) {
            console.log('Error');
            console.log(response);
        }
    });
}

function WorkWithCountries(data) {
    var box = $('[data-name="country"]');
    var list = $(box).find('[data-country="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in data){
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',data[key]['id']);
        clone.find('[data-box="title"]').html(data[key]['title']);
        if( Boolean(data[key]['df']) == true){
            form.country = data[key]['id'];
            $(box).find('[data-country="title"]').html(data[key]['title']);
        }
        clone.click(function() {
            SelectProcessing(this);
        });
        $(list).append(clone);
    }
    makeSelected(box,form.country);
}

var Starter = new function(){
    
    this.vStart = false;
    this.vStop = false;
    this.i = 0;
    this.interval = null;
    this.loader = false;
    this.loaderB = $('[data-id="loader"]');
    
    this.init = function() {
        
    };
    
    this.start = function() {
        this.i = 0;
        this.vStart = true;
        this.interval = setInterval(this.runInterval,10);
    };
    
    this.runInterval = function() {
        if( Starter.i >= 50 && !Starter.loader){
            $(Starter.loaderB).css({display:'block'});
            Starter.loader = true;
        } else {
            Starter.i++;
        }
    };
    
    this.stop = function() {
        if( Starter.loader ){
            $(Starter.loaderB).css({display:'none'});
        }
        this.loader = false;
        clearInterval(this.interval);
    }
    
}

var RenewLoad = false;
function RenewFrom(type,needLoader) {
    var loader = $('[data-id="loader"]');
    form.type = type;
    $.ajax({
        type : 'post',
        url : location.origin + '/getForm',
        dataType : 'json',
        data: form,
        beforeSend : function() {
            if(RenewLoad == true){
                return false;
            }
            RenewLoad = true;
            if(needLoader){
                $(loader).css({display:'block'});
            } else {
                Starter.start();
            }
        },
        complete : function() {
            RenewLoad = false;
            if(needLoader){
                $(loader).css({display:'none'});
            } else {
                Starter.stop();
            }
        },
        success: function(response){
            
            if(typeof(response.parameters) == 'object' && typeof(response.parameters['wAvia']) == 'number'){
                if( form.wAvia != response.parameters['wAvia'] ){
                    if(response.parameters['wAvia'] == 1){
                        AviaSwitcher.set(true);
                    } else {
                        AviaSwitcher.set(false);
                    }
                }
            }
            
            if( typeof(response['depCities']) == 'object' ){
                WorkWithCities(response['depCities']);
            }
            if(typeof(response['FlyDate']) == 'object'){
                RenewCalendarWithFlyDates(response['FlyDate']);
            }
            if( typeof(response['parameters']) == 'object' && RenewNightsList(response['nightsList']) ){
                RenewParameters(response['parameters']);
            }
            if( typeof(response['spoList']) == 'object' ){
                WorkWithSpo(response['spoList']);
            }
            if( typeof(response['TourTypesList']) == 'object' ){
                WorkWithTourTypes(response['TourTypesList']);
            }
            if(response['hotelsList']){
                WorkWithHotels(response['hotelsList']);
            }
            if(response['citiesList']){
                WorkWithArivalCities(response['citiesList']);
            }
            if(response['resortsList']){
                WorkWithArivalResorts(response['resortsList']);
            }
            FillCitiResort();
//            console.log(cities);
            
        },
        error: function(){
            console.log('complete: error');
        }
    });
}
var citiesSorted = new Object();
var cities = new Object();
function WorkWithArivalCities(data) {
    if( typeof(data) == 'object' ){
        cities = new Object();
        citiesSorted = new Object();
        for(var key in data){
            var item = data[key],
                    id = item['id'];
            if( typeof(cities[id]) == 'undefined' ){
                citiesSorted[key] = id;
                cities[id] = {
                    id: id,name : '',search : '',selected : 0,resorts : {}
                };
            }
            cities[id]['name'] = item['title'];
            cities[id]['search'] = item['search'];
        }
    }
}

var resorts = new Object();
function WorkWithArivalResorts(data) {
    if( typeof(data) == 'object' ){
        resorts = new Object();
        for(var key in data){
            var item = data[key],
                    id = item['id'],
                    cityId = item['cityId'];
            if( typeof(resorts[id]) == 'undefined' ){
                resorts[id] = {
                    id: id,name : '',search : '',selected : 0
                };
                cities[cityId]['resorts'][key] = {
                    id : id,selected : 0
                };
            }
            resorts[id]['name'] = item['title'];
            resorts[id]['search'] = item['search'];
        }
    }
}

function FillCitiResort() {
    var MainBranchClone = $('[data-template="TreeFirstItem"]').find('[data-tpl="item"]').clone(true),
        SecondBranchClone = $('[data-template="TreeSecondItem"]').find('[data-tpl="item"]').clone(true),
        ResortBox = $('[data-name="Resorts"]'),
        ResortList = $('[data-name="Resorts"]').find('[data-resorts="list"]');
    $(ResortList).children('li').remove();
    for (var key in citiesSorted){
        var cityItem = cities[citiesSorted[key]],
                resortList = cities[citiesSorted[key]]['resorts'];
        var MainBranch = $(MainBranchClone).clone(true);
        $(MainBranch).find('[data-box="title"]').html(cityItem['name']);
        $(MainBranch).find('[data-box="active"]').attr('data-val',cityItem['id']);
        
        for(var rkey in resortList){
            var resortItem = resorts[resortList[rkey]['id']];
            var SecondBranch = $(SecondBranchClone).clone(true);
            $(SecondBranch).find('[data-box="title"]').html(resortItem['name']);
            $(SecondBranch).find('[data-box="active"]').attr('data-val',resortItem['id']);
            
            $(MainBranch).find('[data-box="list"]').append(SecondBranch);
        }
        
        $(ResortList).append(MainBranch);
    }
    $(ResortBox).find('.tour-scrollbar').data("plugin_tinyscrollbar").update();
}

function makeSelected(list,val) {
    $(list).find('ul').children().each(function() {
        if( $(this).attr('data-val') == val ){
            $(this).addClass('selected');
        } else if( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
    });
}

function SelectProcessing(target) {
    var parent = $(target).parents('[data-name]'),dName = $(parent).attr('data-name'),val = $(target).attr('data-val');
    switch (dName) {
        case 'country': {
            if(val != form.country){
                form.country = val;
                RenewFrom('renewCntr',true);
            }
            break;
        }
        case 'city': {
            if(val != form.city){
                form.city = val;
            }
            break;
        }
        case 'spo': {
            if( val != form.spo ){
                form.spo = val;
            }
            break;
        }
        case 'tourType': {
            if( val != form.ttour ){
                form.ttour = val;
            }
            break;
        }
        default:{
            break;
        }
    }
    makeSelected(parent,val);
}

function WorkWithCities(cities) {
    var box = $('[data-name="city"]');
    var list = $(box).find('[data-city="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in cities){
        var itm = cities[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == "true"){
            form.city = itm['id'];
            $(box).find('[data-city="title"]').html(itm['title']);
        }
        clone.click(function() {
            SelectProcessing(this);
        });
        $(list).append(clone);
    }
    makeSelected(box,form.city);
}

function RenewCalendarWithFlyDates(parameters) {
    calendar.picker({
        yellow: (parameters['yellow'])?parameters['yellow']:[],
        green: (parameters['normal'])?parameters['normal']:[],
        stop: (parameters['stopsale'])?parameters['stopsale']:[]
    },{},$('[data-name="dateFrom"], [data-name="dateTo"]'));
}

function RenewParameters(parameters) {
    if(parameters['defaultDateFromFmt']){
        calendar.setDate(parameters['defaultDateFromFmt'],$('[data-name="dateFrom"]'));
        form.dateFrom = parameters['defaultDateFromFmt'];
    }
    if(parameters['defaultDateToFmt']){
        calendar.setDate(parameters['defaultDateToFmt'],$('[data-name="dateTo"]'));
        form.dateTo = parameters['defaultDateToFmt'];
    }
    
    if(parameters['defaultFrom']){
        form.nightsFrom = parameters['defaultFrom'];
        $('[data-name="nightsFrom"]').find('[data-nightsFrom="title"]').html(parameters['defaultFrom']);
        makeSelected($('[data-name="nightsFrom"]'),form.nightsFrom);
    }
    if(parameters['defaultTo']){
        form.nightsTo = parameters['defaultTo'];
        $('[data-name="nightsTo"]').find('[data-nightsTo="title"]').html(parameters['defaultTo']);
        makeSelected($('[data-name="nightsTo"]'),form.nightsTo);
    }
}

function RenewNightsList(parameters) {
    if(typeof(parameters) == 'object'){
        var nightsFromBox = $('[data-name="nightsFrom"]');
        var nightsToBox = $('[data-name="nightsTo"]');
        var nightsFromList = $(nightsFromBox).find('[data-nightsFrom="list"]');
        var nightsToList = $(nightsToBox).find('[data-nightsTo="list"]');
        $(nightsFromList).find('[data-tpl="item"]').remove();
        $(nightsToList).find('[data-tpl="item"]').remove();
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        for(var key in parameters){
            var itm = parameters[key];
            var nfClone = clone.clone(true);
            var ntClone = clone.clone(true);
            $(nfClone).click({type:'from',v:itm},NightsSelect);
            $(ntClone).click({type:'to',v:itm},NightsSelect);
            nfClone.attr('data-val',itm);
            nfClone.find('[data-box="title"]').html(itm);
            ntClone.attr('data-val',itm);
            ntClone.find('[data-box="title"]').html(itm);
            $(nightsFromList).append(nfClone.clone(true));
            $(nightsToList).append(ntClone.clone(true));
        }
    }
    return true;
}

function NightsSelect(parameters) {
    if(parameters.data.type == 'from'){
        form.nightsFrom = parameters.data.v;
    }
    if(parameters.data.type == 'to'){
        form.nightsTo = parameters.data.v;
    }
    CheckNights(parameters.data.type);
}

function CheckNights(type) {
    if( type == 'from' && parseInt(form.nightsFrom) > parseInt(form.nightsTo) ){
        form.nightsTo = form.nightsFrom;
        $('[data-name="nightsTo"]').find('[data-nightsto="title"]').html(form.nightsTo);
    }
    if( type == 'to' && parseInt(form.nightsTo) < parseInt(form.nightsFrom) ){
        form.nightsFrom = form.nightsTo;
        $('[data-name="nightsFrom"]').find('[data-nightsfrom="title"]').html(form.nightsFrom);
    }
}

function WorkWithSpo(spo) {
    var box = $('[data-name="spo"]');
    var list = $(box).find('[data-spo="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in spo){
        var itm = spo[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == "true"){
            form.spo = itm['id'];
            $(box).find('[data-spo="title"]').html(itm['title']);
        }
        clone.click(function() {
            SelectProcessing(this);
        });
        $(list).append(clone);
    }
    makeSelected(box,form.spo);
}

function WorkWithTourTypes(data) {
    var box = $('[data-name="tourType"]');
    var list = $(box).find('[data-tourType="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in data){
        var itm = data[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == "true"){
            form.ttour = itm['id'];
            $(box).find('[data-tourType="title"]').html(itm['title']);
        }
        clone.click(function() {
            SelectProcessing(this);
        });
        $(list).append(clone);
    }
    makeSelected(box,form.ttour);
}
var hotels = {};
function WorkWithHotels(obj) {
    hotels = {};
    var box = $('[data-name="Hotels"]');
    var list = $(box).find('[data-hotels="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in obj){
        var itm = obj[key];
        var clone = $('[data-template="HotelsItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['nid']);
        clone.attr('data-resot',itm['resortId']);
        clone.attr('data-search',itm['search']);
        clone.attr('data-stars',hotelMask[itm['stars']]);
        clone.find('[data-box="title"]').html(itm['title'] + ", " + itm['stars']);
        if( typeof(hotels[itm['id']]) != 'object' ){
            hotels[itm['nid']] = {
                title : itm['title'],resortid: itm['resortId'],starid: hotelMask[itm['stars']],selected : false
            };
        }
        $(list).append(clone);
    }
    $(box).find('.tour-scrollbar').data("plugin_tinyscrollbar").update();
}

function isResortSelected() {
    var result = false;
    for(var key in resorts){
        if( resorts[key]['selected'] == 1){
            result = true;
            break;
        }
    }
    return result;
}

function isHotelSelected() {
    var result = false;
    for(var key in hotels){
        if( hotels[key]['selected'] == 1){
            result = true;
            break;
        }
    }
    return result;
}

function getHotelsByResorts(data) {
    var list = [];
    var starArr = checkHotelStars(true);
    for(var key in hotels){
        if( $.inArray(hotels[key]['resortid'],data) != -1 && (!isStarSelected || (isStarSelected && $.inArray(hotels[key]['starid'],starArr) != -1) ) ){
            list.push(key);
        }
    }
    return list;
}

function getHotelsList() {
    var list = [];
    var starArr = checkHotelStars(true);
    for(var key in hotels){
        if( hotels[key]['selected'] == 1 && $.inArray(key,form.hotels) == -1 && (!isStarSelected || (isStarSelected && $.inArray(hotels[key]['starid'],starArr) != -1) )){
            list.push(key);
        }
    }
    return list;
}

function getHotelsInResort(data) {
    var list = [];
    var starArr = checkHotelStars(true);
    for(var key in hotels){
        if( hotels[key]['selected'] && $.inArray(hotels[key]['resortid'],data) != -1 ){
            if( (isStarSelected && $.inArray(hotels[key]['starid'],starArr) != -1) || !isStarSelected ){
                list.push(key);
            }
        }
    }
    if(list.length == 0){
        list = getHotelsByResorts(data);
    }
    return list;
}
function getHotelsByStar() {
    
    var list = [];
    var starArr = checkHotelStars(true);
    for(var key in hotels){
        if( $.inArray(hotels[key]['starid'],starArr) != -1 ){
            list.push(key);
        }
    }
    return list;
}


var getFind = false;
$(document).on('click','[data-btn="GetFind"]',function() {
    var btn = $(this),text = $(btn).html(),loader = $('[data-id="loader"]'),loader = $('[data-id="loader"]'),
        hotelSel = false,resortSel = false;
    
    // get data
    var adult = $('[data-form="adult"]').html();
    var child = $('[data-form="child"]').html();
    form.adult = adult;
    form.child = child;
    $('[name="checkbox-meal"]').each(function() {
        var status = $(this).prop('checked'),name = $(this).attr('data-name');
        if( status ){
            form[name] = true;
        } else {
            form[name] = false;
        }
    });
    $('[name="checkbox-3"]').each(function() {
        var status = $(this).prop('checked'),name = $(this).attr('data-name');
        if( status ){
            form[name] = true;
        } else {
            form[name] = false;
        }
    });
    if(isResortSelected()){
        resortSel = true;
    }
    if(isHotelSelected()){
        hotelSel = true;
    }
    
    form.resorts = [];
    for(var key in resorts){
        if( resorts[key]['selected'] == 1 && $.inArray(key,form.resorts) == -1){
            form.resorts.push(key);
        }
    }
    
    form.hotels = [];
    if(resortSel && !hotelSel){
        form.hotels = getHotelsByResorts(form.resorts);
    }
    if(resortSel && hotelSel){
        form.hotels = getHotelsInResort(form.resorts);
    }
    if( !resortSel && hotelSel ){
        form.hotels = getHotelsList();
    }
    if( !resortSel && !hotelSel && isStarSelected){
        form.hotels = getHotelsByStar();
    }
    
    
//    form.resorts = JSON.stringify(form.resorts);
//    form.hotels = JSON.stringify(form.hotels);
    
    var wBlock = $('[data-box="content"]');
    
    $.ajax({
        type : 'post',
        url : location.origin + '/find',
        dataType : 'json',
        data : form,
        beforeSend : function() {
            if(getFind){
                return false;
            }
            getFind = true;
            $(loader).css({display:'block'});
        },
        complete : function(response) {
            getFind = false;
            $(loader).css({display:'none'});
        },
        success: function(response){
            wBlock.find('[data-tpl="item"]').remove();
            if(response['error']){
                
            } else if( typeof(response['list']) == 'object' ) {
                for(var key in response.list){
                    var itm = response.list[key];
                    var clone = $('[data-template="ResultItem"]').find('[data-tpl="item"]').clone(true);
                    clone.attr('data-val',key);
                    if(itm['od'] == 'true'){
                        clone.addClass('tour-search__row-green');
                    }
                    clone.find('[data-id="number"]').html(itm['number']);
                    clone.find('[data-id="DatesStr"]').html(itm['DatesStr']);
                    clone.find('[data-id="nights"]').html(itm['nights']);
                    clone.find('[data-id="hotel"]').html(itm['hotel']);
                    clone.find('[data-id="resort"]').html(itm['resort']);
                    clone.find('[data-id="meal"]').html(itm['meal']);
                    clone.find('[data-id="nomer"]').html(itm['nomer']);
                    clone.find('[data-id="spo"]').html(itm['spo']);
                    clone.find('[data-id="priceFmt"]').html(itm['priceCoef'] + itm['curr']);
                    
                    clone.find('[data-id="hotelPlaces"]').html(itm['hotel_places']);
                    clone.find('[data-id="aviaPlaces"]').html(itm['avia_e_to'] + '/' + itm['avia_e_from']);
                    $(wBlock).append(clone);
                }
                resize_canvas();
            }
        },
        error: function(response) {
            getFind = false;
        }
    });
});