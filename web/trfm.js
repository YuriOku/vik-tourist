//console.log(window);
//console.log(window['tf'].q);
// everything is wrapped in the XD function to reduce namespace collisions
var XD = function(){
  
    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;
    
    return {
        postMessage : function(message, target_url, target) {
//            console.log('postMessage From trfm.js' + message + ' target: ' + target_url);
//            console.log('target trfm.js: '+target.toString());
            
            if (!target_url) { 
                return; 
            }
    
            target = target || parent;  // default to parent
    
            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target['postMessage'](message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // the browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        },
  
        receiveMessage : function(callback, source_origin) {
//            console.log('receiveMessage From trfm.js');
//            console.log('callback trfm.js: '+callback.toString());
//            console.log('source_origin trfm.js: '+source_origin.toString());
            
            // browser supports window.postMessage
            if (window['postMessage']) {
                // bind the callback to the actual event associated with window.postMessage
                if (callback) {
                    attached_callback = function(e) {
                        if ((typeof source_origin === 'string' && e.origin !== source_origin)
                        || (Object.prototype.toString.call(source_origin) === "[object Function]" && source_origin(e.origin) === !1)) {
                            return !1;
                        }
                        callback(e);
                    };
                }
                if (window['addEventListener']) {
                    window[callback ? 'addEventListener' : 'removeEventListener']('message', attached_callback, !1);
                } else {
                    window[callback ? 'attachEvent' : 'detachEvent']('onmessage', attached_callback);
                }
            } else {
                // a polling loop is started & callback is called whenever the location.hash changes
                interval_id && clearInterval(interval_id);
                interval_id = null;

                if (callback) {
                    interval_id = setInterval(function(){
                        var hash = document.location.hash,
                        re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({data: hash.replace(re, '')});
                        }
                    }, 100);
                }
            }   
        }
    };
}();

(function(c) {
    var Util = {
            extendObject: function(a, b) {
                for(prop in b){
                    a[prop] = b[prop];
                }
                return a;
            },
            proto: 'https:' == document.location.protocol ? 'https://' : 'http://'
	}

	var options = Util.extendObject({
		id: 0,
		domain: "turistas.ddns.net",
		bg_color: "FFFFFF"
	}, c);

	options.widget_url = [Util.proto, options.domain, "/index.html?", "id=", options.id, "&t=", Math.random()].join("");
	options.widget_url += "#" + encodeURIComponent(document.location.href);

	Widget = {
		created: false,
		widgetElement: null,
		show: function() {
			if (this.created)
				return;
			this.widgetElement = document.createElement('div');
			this.widgetElement.setAttribute('id', 'widget_container');
			this.widgetElement.innerHTML = '<iframe id="widget_iframe" src="' + options.widget_url + '" scrolling="no" width="100%" height="0" frameborder="0"></iframe>';
			document.getElementById("TF59534033").appendChild(this.widgetElement);
			this.widgetElement.style.display = 'block';
			this.created = true;
		}
	}

	XD.receiveMessage(function(message) {
		if (message.data > 0 && document.getElementById("widget_iframe")){
			document.getElementById("widget_iframe").height = message.data + 'px';
		}
	}, Util.proto + options.domain);

	Widget.show();
})({id: 1,bg_color: 'FF0000'});