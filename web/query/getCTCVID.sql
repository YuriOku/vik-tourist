SELECT
T1._IDRRef,
1 as fldT
FROM dbo._Reference52947 T1
WHERE (T1._Fld52950RRef = {country}) AND (T1._Fld52951RRef = {city})
union all
SELECT T1._IDRRef,
2 as fldT
FROM dbo._Reference15452 T1 
WHERE (T1._Fld54907 = 0x01)
union all
SELECT
T1._IDRRef,
3 as fldT
FROM dbo._Reference15452 T1
INNER JOIN dbo._Reference15452 T2
ON (T1._OwnerIDRRef = T2._OwnerIDRRef)
WHERE (T2._IDRRef = {city}) AND (T1._Fld54906 = 0x01)