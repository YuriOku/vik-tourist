SELECT
T3._Fld49580RRef,
T3._Fld53047RRef,
T3._Fld53048RRef,
T3._Fld53049RRef,
T3._Fld53081RRef,
T41._Description as Name_Fld49580RRef,
T42._Description as Name_Fld53047RRef,
T43._Description as Name_Fld53048RRef,
T44._Description as Name_Fld53049RRef,
T45._Description as Name_Fld53081RRef
FROM dbo._InfoRg55153 T1
LEFT OUTER JOIN dbo._Reference52949 T2
ON (T1._Fld55155RRef = T2._IDRRef) AND (T2._Fld60937 = 0)
LEFT OUTER JOIN dbo._Reference48820 T3
ON (T2._Fld52954RRef = T3._IDRRef) AND (T3._Fld60937 = 0)
LEFT OUTER JOIN dbo._Reference49578 T41
ON (T3._Fld49580RRef = T41._IDRRef)
LEFT OUTER JOIN dbo._Reference49578 T42
ON (T3._Fld53047RRef = T42._IDRRef)
LEFT OUTER JOIN dbo._Reference49578 T43
ON (T3._Fld53048RRef = T43._IDRRef)
LEFT OUTER JOIN dbo._Reference49578 T44
ON (T3._Fld53049RRef = T44._IDRRef)
LEFT OUTER JOIN dbo._Reference49578 T45
ON (T3._Fld53081RRef = T45._IDRRef)
WHERE ((T1._Fld60937 = 0)) 
AND (
(T1._Fld55154RRef in ({ctcvid})) AND 
((T1._Fld55156 >= '{dateFrom}') AND (T1._Fld55156 <= '{dateTo}')) AND 
((T1._Fld55157 >= {nightsFrom}) AND (T1._Fld55157 <= {nightsTo}))
)
GROUP BY T3._Fld49580RRef,
T3._Fld53047RRef,
T3._Fld53048RRef,
T3._Fld53049RRef,
T3._Fld53081RRef,
T41._Description,
T42._Description,
T43._Description,
T44._Description,
T45._Description