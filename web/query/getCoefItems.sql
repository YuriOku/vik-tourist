SELECT
CONVERT(varchar, T1.Fld56508_, 4) as date,
T1.Fld56509RRef as spo,
T1.Fld54965_ as k,
T1.Fld56543_ as kPerPerson_1,
T1.Fld56562_ as kPerPerson_2,
T1.Fld56563_ as kPerPerson_3,
T1.Fld56809_ AS kPerPerson_4,
T1.Fld56810_ AS kPerPerson_5,
T1.Fld56811_ AS kPerPerson_6,
T1.Fld56812_ AS kPerPerson_7,
T1.Fld56813_ AS kPerPerson_8,
T1.Fld56814_ AS kPerPerson_9,
T1.Fld56815_ AS kPerPerson_10
FROM (SELECT
T4._Fld56509RRef AS Fld56509RRef,
T4._Fld54965 AS Fld54965_,
T4._Fld56508 AS Fld56508_,
T4._Fld56543 AS Fld56543_, --1
T4._Fld56562 AS Fld56562_, --2
T4._Fld56563 AS Fld56563_, --3
T4._Fld57733 AS Fld56809_, --4
T4._Fld57734 AS Fld56810_, --5
T4._Fld57735 AS Fld56811_, --6
T4._Fld57736 AS Fld56812_, --7
T4._Fld57737 AS Fld56813_, --8
T4._Fld57738 AS Fld56814_, --9
T4._Fld57739 AS Fld56815_  --10
FROM (SELECT
T3._Fld55129RRef AS Fld55129RRef,
T3._Fld56508 AS Fld56508_,
T3._Fld56509RRef AS Fld56509RRef,
MAX(T3._Period) AS MAXPERIOD_
FROM dbo._InfoRg54964 T3
WHERE T3._Period <= DATEADD (year , 2000 , GETDATE() ) AND ((((T3._Fld55129RRef = {country}) AND ((T3._Fld56508 >= '{dateFrom}') AND (T3._Fld56508 <= '{dateTo}')))))
GROUP BY T3._Fld55129RRef,
T3._Fld56508,
T3._Fld56509RRef) T2
INNER JOIN dbo._InfoRg54964 T4
ON T2.Fld55129RRef = T4._Fld55129RRef AND T2.Fld56508_ = T4._Fld56508 AND T2.Fld56509RRef = T4._Fld56509RRef AND T2.MAXPERIOD_ = T4._Period) T1