SELECT TOP {TOP}
T25._Fld54875 as hotelPriority,
T333._Fld55685,
T22._Fld52954RRef AS Q_001_F_000RRef, --втаб.Расчет.Тур
T23._Fld52950RRef AS Q_001_F_001RRef, --втаб.СтранаГородОтправления.Страна
CONVERT(varchar, T1.{_Fld52990}, 4) AS datefrom, --втаб.Дата КАК ДатаКалендаря
convert(varchar, T1.{_Fld52990}, 112) as dateFromBonus,
T1.{_Fld52989} AS Q_001_F_003_, --втаб.Продолжительность КАК Продолжительность
T1.{_Fld52988RRef} as komb,
CONVERT(varchar, DATEADD(DAY,(T5._Fld52499 - 1.0),T1.{_Fld52990}), 4) AS dateto, --датаприлета
T5._Fld52499 AS days2,
--T8._Fld49400RRef AS Q_001_F_004RRef, --втаб.КомбинацияПроживанияВТурах.Отель1
--T8._Fld49401RRef AS Q_001_F_005RRef, --втаб.КомбинацияПроживанияВТурах.ВидПроживания1
--T8._Fld49420RRef AS Q_001_F_006RRef, --втаб.КомбинацияПроживанияВТурах.Питание1
	CASE WHEN (T25._Fld54875 = 0.0) THEN T8._Fld49400RRef ELSE T8._Fld49402RRef END as Q_001_F_004RRef,
	CASE WHEN (T25._Fld54875 = 0.0) THEN T8._Fld49401RRef ELSE T8._Fld49403RRef END AS Q_001_F_005RRef,
	CASE WHEN (T25._Fld54875 = 0.0) THEN T8._Fld49420RRef ELSE T8._Fld49421RRef END AS Q_001_F_006RRef,
T10._Fld15728RRef AS nomerClass,
T24._OwnerIDRRef AS Q_001_F_007RRef, --втаб.КомбинацияПроживанияВТурах.Отель1.Владелец КАК Город
T1.{_Fld52995} AS Q_001_F_008_, --втаб.Цена КАК Цена
{priceInValutaSting} AS priceInValuta, --втаб.Цена * &КурсВалютыТура / &КурсВалютВыбраннойВалюты КАК ЦенаВВыбраннойВалюте
T25._Fld48823RRef AS Q_001_F_009RRef, --втаб.Расчет.Тур.Валюта КАК ТурВалюта
--(ISNULL(CAST(T3.Fld50605Turnover_ AS NUMERIC(22, 0)),0.0) - ISNULL(CAST(T3.Fld50691Turnover_ AS NUMERIC(22, 0)),0.0)) AS aviaeconom, --МестАвиаСвободноЭконом
--(ISNULL(CAST(T6.Fld50605Turnover_ AS NUMERIC(22, 0)),0.0) - ISNULL(CAST(T6.Fld50691Turnover_ AS NUMERIC(22, 0)),0.0)) AS aviaeconomback, --МестАвиаСвободноЭкономОбратно
T333._Fld54310 AS aviaeconom,  --СвободноМестТуда
T333._Fld54312 AS aviaeconomback, --СвободноМестОбратно
T25._Description AS d1, --втаб.Расчет.Тур.Наименование
T25._Fld55045,
T26._Description AS d2, --втаб.СтранаГородОтправления.Страна.Наименование
T26._Fld55021 as d21, --польское название страны
T24._Description AS d3, --втаб.КомбинацияПроживанияВТурах.Отель1.Наименование
T24._Fld15703RRef as hotelNetwork,
T10._Description AS d4, --втаб.КомбинацияПроживанияВТурах.ВидПроживания1.Наименование
T27._Description AS d5, --втаб.КомбинацияПроживанияВТурах.Питание1.Наименование

CASE
	WHEN T27._Description = 'RO' THEN 8
	WHEN T27._Description = 'BB' THEN 7
	WHEN T27._Description = 'HB' THEN 6
	WHEN T27._Description = 'HB+' THEN 5
	WHEN T27._Description = 'FB' THEN 4
	WHEN T27._Description = 'FB+' THEN 3
	WHEN T27._Description = 'AI' THEN 2
	WHEN T27._Description = 'UAI' THEN 1
END AS eatIntType,

T28._Description AS d6, --втаб.КомбинацияПроживанияВТурах.Отель1.Владелец.Наименование
T28._Fld55044 as d61,
T29._Description AS d7, --втаб.Расчет.Тур.Валюта.Наименование
T30._Description AS d8, --втаб.Рейсы.РейсТуда.Наименование
(((ISNULL(CAST(T9._Fld53909 AS NUMERIC(3, 0)),0.0) + ISNULL(CAST(T11._Fld53097 AS NUMERIC(3, 0)),0.0)) + ISNULL(CAST(T12._Fld53902 AS NUMERIC(3, 0)),0.0)) + ISNULL(CAST(T13._Fld53894 AS NUMERIC(3, 0)),0.0)) AS hotelfree, --ТПГ_БронированиеМестОтелиОбороты.СвободноОтель КАК МестОтельСвободно
--(ISNULL(CAST(T3.Fld51284Turnover_ AS NUMERIC(22, 0)),0.0) - ISNULL(CAST(T3.Fld51285Turnover_ AS NUMERIC(22, 0)),0.0)) AS aviabusines, --МестАвиаСвободноБизнес
--(ISNULL(CAST(T6.Fld51284Turnover_ AS NUMERIC(22, 0)),0.0) - ISNULL(CAST(T6.Fld51285Turnover_ AS NUMERIC(22, 0)),0.0)) AS aviabusinesback, --МестАвиаСвободноБизнесОбратно
0 AS aviabusines,
0 AS aviabusinesback,
--T14.Fld51075_ AS stopavia, --ТПГ_StopSaleАвиаСрезПоследних.StopSale КАК StopSaleАвиа
--T18.Fld51075_ AS stopaviato, --ТПГ_StopSaleАвиаСрезПоследнихОбратно.StopSale
T333._Fld54311 AS stopavia, --СтопСейлТуда
T333._Fld54313 AS stopaviato,  ---СтопСейлОбратно
(CASE WHEN (((T9._Fld53910 = 0x01 OR T11._Fld53098 = 0x01) OR T12._Fld53903 = 0x01) OR T13._Fld53895 = 0x01) THEN 0x01 WHEN NOT (((T9._Fld53910 = 0x01 OR T11._Fld53098 = 0x01) OR T12._Fld53903 = 0x01) OR T13._Fld53895 = 0x01) THEN 0x00 END) AS hotelstop, --ТПГ_БронированиеМестОтелиОбороты.StopSale КАК StopSaleОтель
T24._Fld49491RRef, --втаб.КомбинацияПроживанияВТурах.Отель1.Курорт
T31._Description AS d9, --втаб.КомбинацияПроживанияВТурах.Отель1.Курорт.Наименование
T31._Fld55051,
T25._Fld52346, --втаб.Расчет.Тур.СпецПредложение КАК СпецПредложение
T25._Fld50910, --втаб.Расчет.Тур.УслугиВключитьПроживание
T25._Fld50911, --втаб.Расчет.Тур.УслугиВключитьАвиаперелет
T25._Fld50912, --втаб.Расчет.Тур.УслугиВключитьТрансфер
T25._Fld50913, --втаб.Расчет.Тур.УслугиВключитьЭкскурсионнаяПрограмма
T25._Fld50914, --втаб.Расчет.Тур.УслугиВключитьМедицинскаяСтраховка
T25._Fld50915, --втаб.Расчет.Тур.УслугиВключитьВиза
T25._Fld53082RRef as tourtype,
T2._IDRRef as flightsId, -- втаб.Рейсы.ID
T2._Fld52952RRef AS outbound, --втаб.Рейсы.РейсТуда
T2._Fld52953RRef AS flightback, --втаб.Рейсы.РейсОбратно
T32._Description AS d10, --втаб.Рейсы.РейсОбратно.Наименование
T1.{_Fld52991RRef} AS ref11, --втаб.КомбинацияПроживанияВТурах
T8._Description, --втаб.КомбинацияПроживанияВТурах.Наименование
T27._Fld51355, --втаб.КомбинацияПроживанияВТурах.Питание1.КодМеждународный
T25._Fld50900, --втаб.Расчет.Тур.ТарифАвиа
T25._Fld50901, --втаб.Расчет.Тур.ТарифСтандарт
T25._Fld50902 as ebb, --втаб.Расчет.Тур.ТарифРаннееБронирование
T30._Fld15713RRef, --втаб.Рейсы.РейсТуда.АвиаПеревозчик
T33._Description AS d11, --втаб.Рейсы.РейсТуда.АвиаПеревозчик.Наименование
T33._Fld15719,
T32._Fld15713RRef, --втаб.Рейсы.РейсОбратно.АвиаПеревозчик
T32._Description AS d10, --втаб.Рейсы.РейсОбратно.Наименование КАК РейсОбратноНаименование1
T34._Description AS d14, --втаб.КомбинацияПроживанияВТурах.Отель1.КатегорияОтеля.Наименование
CASE WHEN (T25._Fld49606RRef = {PUSTO}) THEN 0x00 ELSE 0x01 END AS existTour, --ЕстьОписаниеТур
CASE WHEN (T24._Fld49509RRef = {PUSTO}) THEN 0x00 ELSE 0x01 END AS existHotel, --ЕстьОписаниеОтель
T24._Fld52759, --втаб.КомбинацияПроживанияВТурах.Отель1.НеДляРусских
T8._Description  AS d12, --КомбинацияПроживанияВТурах
T32._Fld15713RRef, --втаб.Рейсы.РейсОбратно.АвиаПеревозчик КАК РейсОбратноАвиаПеревозчик1
T35._Description AS d15, --втаб.Рейсы.РейсОбратно.АвиаПеревозчик.Наименование КАК АвиаПеревозчикОбратно
T35._Fld15719,
CASE WHEN (T8._Fld49402RRef = {PUSTO}) THEN 0x00 ELSE 0x01 END AS morehotel, --БольшеОдногоОтеля
T25._Fld52789, --втаб.Расчет.Тур.УслугиВключитьАвтобус
T25._Fld52791, --втаб.Расчет.Тур.УслугиВключитьВнутреннийАвиаПерелет
T25._Fld52793, --втаб.Расчет.Тур.УслугиВключитьДопУслуги
T22._Fld52956 as Raschet_id,
T1.{_Fld52994RRef} as days3,
T25._Fld53083 as nightly,
T25._Fld53084 as countryWeek,
T28._OwnerIDRRef as hotelCountry,
T5._Fld52499 as inDays,

T25._Fld54181 as TarifBonusNight,
T25._Fld54182 as TarifUpgradeEat,
T25._Fld54183 as TarifChildren,
T25._Fld54184 as TarifSkidaka,
T25._Fld54185 as TarifNomer,
T25._Fld54186 as TarifFreeTransfer,
T25._Fld53511 as TarifWnight,
T25._Fld52346 as TarifSpecAtten,
T25._ParentIDRRef as spoId,
T36._Description as desc1,
T37._Description as desc2,
hotelGroups._Description as holesInTour,

hotelGroups._IDRRef as hotelsGroupIdx,

hotelGroups._Fld55693RRef as secondHotelIdx,
T24_secondHotel._Description as secondHotelTitle,
hotelGroups._Fld55694RRef as thirdHotelIdx,
T24_thirdHotel._Description as thirdHotelTitle,

T26._Fld55015 --галкa в страну прятать Вид номера в Виде проживания
from dbo.{table_name} T1

LEFT OUTER JOIN dbo._Reference55689 as hotelGroups
ON T1.{_Fld52988RRef_1} = hotelGroups._IDRRef

LEFT OUTER JOIN dbo._Reference52948 T2
ON T1.{_Fld52993RRef} = T2._IDRRef
LEFT OUTER JOIN dbo._Reference52479 T5
ON T1.{_Fld52994RRef} = T5._IDRRef

LEFT OUTER JOIN dbo._InfoRg54306 T333
ON (((T1.{_Fld52993RRef} = T333._Fld54309RRef) 
AND (T1.{_Fld52990} = T333._Fld54307)) 
AND (DATEADD(DAY,(T5._Fld52499 - 1),T1.{_Fld52990}) = T333._Fld54308))

LEFT OUTER JOIN dbo._Reference49399 T8
ON T1.{_Fld52991RRef} = T8._IDRRef
LEFT OUTER JOIN dbo._Reference15607 T10
ON T8._Fld49401RRef = T10._IDRRef
LEFT OUTER JOIN dbo._InfoRg53091 T11
ON (((((T1.{_Fld52990} = T11._Fld53092) AND (T8._Fld49400RRef = T11._Fld53093RRef)) AND (T1.{_Fld52989} = T11._Fld53094)) AND (T10._Fld15726RRef = T11._Fld53095RRef)) AND (T10._Fld15728RRef = T11._Fld53096RRef))


LEFT OUTER JOIN dbo._InfoRg53905 T9
ON (((((T1.{_Fld52990} = T9._Fld53906) AND (T8._Fld49400RRef = T9._Fld53907RRef)) AND (T1.{_Fld52989} = T9._Fld53908))))

LEFT OUTER JOIN dbo._InfoRg53897 T12
ON (((((T1.{_Fld52990} = T12._Fld53898) AND (T8._Fld49400RRef = T12._Fld53899RRef)) AND (T1.{_Fld52989} = T12._Fld53900))) AND (T10._Fld15728RRef = T12._Fld53901RRef))

LEFT OUTER JOIN dbo._InfoRg53889 T13
ON (((((T1.{_Fld52990} = T13._Fld53890) AND (T8._Fld49400RRef = T13._Fld53891RRef)) AND (T1.{_Fld52989} = T13._Fld53892)) AND (T10._Fld15726RRef = T13._Fld53893RRef)))


LEFT OUTER JOIN dbo._Reference52949 T22
ON T1.{_Fld52992RRef} = T22._IDRRef

LEFT OUTER JOIN dbo._Reference52947 T23
ON T1.{_Fld52988RRef} = T23._IDRRef
LEFT OUTER JOIN dbo._Reference15592 T24
ON T8._Fld49400RRef = T24._IDRRef

LEFT OUTER JOIN dbo._Reference15592 T24_secondHotel
ON hotelGroups._Fld55693RRef = T24_secondHotel._IDRRef

LEFT OUTER JOIN dbo._Reference15592 T24_thirdHotel
ON hotelGroups._Fld55694RRef = T24_thirdHotel._IDRRef

LEFT OUTER JOIN dbo._Document48946 T42
ON T22._Fld52955RRef = T42._IDRRef
LEFT OUTER JOIN dbo._Reference48820 T25
ON T22._Fld52954RRef = T25._IDRRef
LEFT OUTER JOIN dbo._Reference15451 T26
ON T23._Fld52950RRef = T26._IDRRef
LEFT OUTER JOIN dbo._Reference15606 T27
ON T8._Fld49420RRef = T27._IDRRef
LEFT OUTER JOIN dbo._Reference15452 T28
ON T24._OwnerIDRRef = T28._IDRRef
LEFT OUTER JOIN dbo._Reference30 T29
ON T25._Fld48823RRef = T29._IDRRef
LEFT OUTER JOIN dbo._Reference15707 T30
ON T2._Fld52952RRef = T30._IDRRef
LEFT OUTER JOIN dbo._Reference49489 T31
ON T24._Fld49491RRef = T31._IDRRef
LEFT OUTER JOIN dbo._Reference15707 T32
ON T2._Fld52953RRef = T32._IDRRef
LEFT OUTER JOIN dbo._Reference15620 T33
ON T30._Fld15713RRef = T33._IDRRef
LEFT OUTER JOIN dbo._Reference15593 T34
ON T24._Fld15600RRef = T34._IDRRef
LEFT OUTER JOIN dbo._Reference15620 T35
ON T32._Fld15713RRef = T35._IDRRef
LEFT OUTER JOIN dbo._Reference15721 T36
ON T10._Fld15727RRef = T36._IDRRef
LEFT OUTER JOIN dbo._Reference15722 T37
ON T10._Fld15728RRef = T37._IDRRef 
WHERE (T22._Fld55521 = 0x00) and {CCTVWhere} and {WHERE}
ORDER BY Q_001_F_008_