SELECT
T2._Fld54132RRef as idx,
T3._Description,
T3._Fld55044,
T3._ownerIDRRef,
t6._Description as countryTitle,
t6._Fld55021
FROM dbo._Reference48820_VT53085 T1
INNER JOIN dbo._Reference48820_VT54130 T2
ON (T1._Reference48820_IDRRef = T2._Reference48820_IDRRef)
LEFT OUTER JOIN dbo._Reference15452 T3
ON T2._Fld54132RRef = T3._IDRRef
LEFT OUTER JOIN dbo._Reference48820 T4
ON T2._Reference48820_IDRRef = T4._IDRRef
left join dbo._Reference15451 as t6 on(t6._IDRRef = T3._ownerIDRRef)
WHERE
(T1._Fld53087RRef = {country})
AND (T4._Fld50900 = 0x01)
AND (T4._Fld52345 = 0x01)
and T3._Fld52241 = 0x00
GROUP BY T2._Fld54132RRef,
T3._ownerIDRRef,
T3._Description,
T3._Fld55044,
t6._Description,
t6._Fld55021