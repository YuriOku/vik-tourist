
var calendar = calendar || {};
(function (calendar) {
    calendar.picker = function (dates,tooltips,target) {
        dates = JSON.parse(JSON.stringify(dates));
        tooltips = tooltips || {};
        $(target).datepicker("destroy");
        $(target).datepicker({
            numberOfMonths: 3,
            minDate: 0,
            dateFormat: 'dd.mm.yy',
            beforeShowDay: function(date) {
               var m = date.getMonth(),d = date.getDate(),y = date.getFullYear();
               if( typeof(dates['green']) == 'object' && dates['green'].length > 0 ){
                    for(var i = 0;i < dates['green'].length;i++) {
                        if($.inArray(d+'.'+(m + 1)+'.'+y, dates['green']) !== -1) {
                            return [true, 'highlight_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                        }
                    }
               }
               if( typeof(dates['yellow']) == 'object' && dates['yellow'].length > 0 ){
                    for(var i = 0;i < dates['yellow'].length;i++) {
                        if($.inArray(d+'.'+(m + 1)+'.'+y, dates['yellow']) !== -1) {
                            return [true, 'yellow_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                        }
                    }
               }
               if( typeof(dates['stop']) == 'object' && dates['stop'].length > 0 ){
                    for(var i = 0;i < dates['stop'].length;i++) {
                        if($.inArray(d+'.'+(m + 1)+'.'+y, dates['stop']) !== -1) {
                            return [true, 'stopsale_date date_tooltip', tooltips[d+'.'+(m + 1)+'.'+y]];
                        }
                    }
               }
               return [true];
            },
            onSelect: function(date,inst) {
                var name = $('#' + inst.id).attr('data-name');
                if(name && typeof(name) == 'string'){
                    form[name] = date;
                }
            }
	});
    };
    
    calendar.setDate = function(date,target) {
        $(target).datepicker('setDate', date);
    };

} (calendar));

var form = {
    country:0,wAvia:false,city:0,spo:0,ttour:0,dateFrom:'',dateTo:'',adult:2,child:0,nightsFrom:7,nightsTo:14,earlyBron:false,
    meal:{ro:false,bb:false,hb:false,'hbplus':false,fb:false,fbplus:false,ai:false,uai:false},
    hotelCat:{2:false,3:false,4:false,5:false,apt:false,villa:false},
    priceFrom:0,priceTo:0,currency:'usd',
    cities:{},hotels:{},resorts:{}
};

$(function(){
//WorkWithCountries(Countries);
AviaSwitcher.set(true);

//    RenewFrom('selectCountry',true);

    $('.tour-scrollbar').tinyscrollbar({thumbSize : 30});

    tour_select.listeners();

    var elem_body = $('body');
    elem_body.on('click',function(event){
        $('.tour-select__list').removeClass('js-select-on');
    });

    // bouble check
    $('.js-checkbox-double').change(function(){
            var status = $(this).prop('checked'),
                    checkbox = $(this).next(),
                    checkbox_list = $(this).parents('.tour-search-list__item')
                                            .find('.tour-search-list__in')
                                            .find('.tour-checkbox__input'),
                    marker = checkbox_list.next();

            checkbox_list.prop('checked', status);

            if(status){
                marker.addClass('js-checked');
                $(checkbox_list).each(function() {
                    var idx = $(this).attr('data-val');
                    form.resorts[idx]['ps'] = 1;
                });
            }else{
                $(checkbox_list).each(function() {
                    var idx = $(this).attr('data-val');
                    form.resorts[idx]['ps'] = 0;
                });
                marker.removeClass('js-checked')
                checkbox.removeClass('js-check-1')
            }
    });

    $('.js-checkbox-list').change(function(){
        var status = $(this).prop('checked'),
            doubl = $(this).parents('.tour-search-list__item').find('.js-checkbox-double'),
            doubl_marker = doubl.next(),
            list = $(this).parents('.tour-search-list__in').find('.tour-checkbox__input'),
            length_list = list.length,
                parentId = $(this).parents('[data-branch="main"]').find('[data-box="active"]').attr('data-val'),
                thisId = $(this).attr('data-val'),
            length_check = list.filter(':checked').length;

            if(length_check == 0){
                doubl_marker.removeClass('js-check-1');
                form.resorts[thisId]['ps'] = 0;
//                form.cities[parentId]['list'][thisId]['ps'] = 0;
            } else if(length_list == length_check){
                doubl.prop('checked', true);
                doubl_marker.removeClass('js-check-1');
                form.resorts[thisId]['ps'] = 1;
//                form.cities[parentId]['list'][thisId]['ps'] = 1;
            } else if(length_list > length_check){
                doubl.prop('checked', false);
                doubl_marker.addClass('js-check-1');
                form.resorts[thisId]['ps'] = 1;
//                form.cities[parentId]['list'][thisId]['ps'] = 1;
            }
    });


    $('.tour-checkbox__input').change(function(){
            var status = $(this).prop('checked'),
                marker = $(this).next();
            if(status){
                    marker.addClass('js-checked')
            }else{
                    marker.removeClass('js-checked')
            }
    });
    

})//end ready


var GetFind = false;
$(document).on('click','[data-btn="GetFind"]',function() {
    var btn = $(this),text = $(btn).html(),loader = $('[data-id="loader"]');
    $.ajax({
        type : 'post',
        url : location.origin + '/trfm_a.php',
        dataType : 'json',
        data: {
            action:'GetFind',trfm:form
        },
        beforeSend : function() {
            if(GetFind == true){
                return false;
            }
            GetFind = true;
            $(btn).html('Загружаем');
            $(loader).css({display:'block'});
        },
        complete : function() {
            GetFind = false;
            $(loader).css({display:'none'});
            $(btn).html(text);
        },
        success: function(response){
            var wBlock = $('[data-box="content"]');
            wBlock.find('[data-tpl="item"]').remove();
            if(response.list){
                for(var key in response.list){
                    var itm = response.list[key];
                    var clone = $('[data-template="ResultItem"]').find('[data-tpl="item"]').clone(true);
                    clone.attr('data-val',key);
                    if(itm['od'] == true){
                        clone.addClass('tour-search__row-green');
                    }
                    clone.find('[data-id="number"]').html(itm['number']);
                    clone.find('[data-id="DatesStr"]').html(itm['DatesStr']);
                    clone.find('[data-id="nights"]').html(itm['nights']);
                    clone.find('[data-id="hotel"]').html(itm['hotel']);
                    clone.find('[data-id="resort"]').html(itm['resort']);
                    clone.find('[data-id="meal"]').html(itm['meal']);
                    clone.find('[data-id="nomer"]').html(itm['nomer']);
                    clone.find('[data-id="spo"]').html(itm['spo']);
                    clone.find('[data-id="priceFmt"]').html(itm['priceFmt']);
                    
                    $(wBlock).append(clone);
                }
            }
        },
        error: function(){
            console.log('complete: error');
        }
    });
});

function WorkWithCountries(data) {
    var box = $('[data-name="country"]');
    var list = $(box).find('[data-country="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in data){
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',data[key]['id']);
        clone.find('[data-box="title"]').html(data[key]['title']);
        if(data[key]['df'] == true){
            form.country = data[key]['id'];
            $(box).find('[data-country="title"]').html(data[key]['title']);
        }
        clone.click(function() {
            SelectProcessing(this);
        });
        $(list).append(clone);
    }
}

function SelectProcessing(target) {
    var parent = $(target).parents('[data-name]');
    if($(parent).attr('data-name') == 'country'){
        var val = $(target).attr('data-val');
        
        if(val != form.country){
            form.country = val;
            RenewFrom('selectCountry');
        }
    }
    
}

$(document).on('click','.tour-select__value',function() {
//    var parent = $(this).parents('[data-name]');
    console.log(2);
//    if($(parent).attr('data-name') == 'country'){
//        console.log(1);
//        var val = $(this).attr('data-val');
//        form.country = val;
//        RenewFrom('selectCountry');
//    }
//    if($(parent).attr('data-name') == 'city'){
//        var val = $(this).attr('data-val');
//        form.city = val;
//        RenewFrom('selectCity');
//    }
});

// SELECT
var $tour_form = $('.tour-container');

var tour_select = {
	select 		: $('.js-select'),
	select_t 	: '.js-select',
	value_t 	: '.tour-select__value',

	init:function(){
		// console.log(this.select);
	},
	listeners:function(){
		$tour_form.on('click', this.select_t, this.showList );
		$tour_form.on('click', this.value_t, this.selectValue );
        $tour_form.on('click','.tour-select__box',function(ev){
            ev.stopImmediatePropagation();
        });
	},
	showList: function(){	
		$(this).next('.tour-select__list').toggleClass('js-select-on');
	},
	selectValue: function(){
		var box = $(this).parents('.tour-select__box'),
			list = box.children('.tour-select__list'),
			select = box.children('.tour-select')

		list.removeClass('js-select-on');
		// Set value text
		select.text( $(this).text() );
	}
}// end tour_select

var AviaSwitcher = AviaSwitcher || {};
(function (switcher) {
    switcher.o = $('[data-switcher="Avia"]');
    
    switcher.o.find('input').on('change',function() {
        if( $(this).prop('checked') ){
            form.wAvia = 1;
        } else {
            form.wAvia = 0;
        }
        RenewFrom('renewWavia');
    });
    
    switcher.set = function(i) {
        if(i == true){
            form.wAvia = true;
            switcher.o.find('input').prop('checked', true);
        } else {
            form.wAvia = false;
            switcher.o.find('input').prop('checked', false);
        }
    };
    
    

} ( AviaSwitcher ));


function RenewFrom(type,needLoader) {
    var loader = $('[data-id="loader"]');
    $.ajax({
        type : 'post',
        url : location.origin + '/trfm_a.php',
        dataType : 'json',
        data: {
            action:'RenewFrom',tp:type,trfm:form
        },
        beforeSend : function() {
            if(needLoader){
                $(loader).css({display:'block'});
            }
        },
        complete : function() {
            if(needLoader){
                $(loader).css({display:'none'});
            }
        },
        success: function(response){
            if( typeof(response['CitiesList']) == 'object' ){
                WorkWithCities(response['CitiesList']);
            }
            
            if(typeof(response['FlyDate']) == 'object'){
                RenewCalendarWithFlyDates(response['FlyDate']);
            }
            
            if( typeof(response['ParametrsList']) == 'object' && RenewNightsList(response['ListNightBySpo']) ){
                RenewParameters(response['ParametrsList']);
            }
            
            if( typeof(response['SpoList']) == 'object' ){
                WorkWithSpo(response['SpoList']);
            }
            if( typeof(response['TourTypesList']) == 'object' ){
                WorkWithTourTypes(response['TourTypesList']);
            }
            if(response['Hotels']){
                WorkWithHotels(response['Hotels']);
            }
            if(response['CitiesResort']){
                WorkWithResorts(response['CitiesResort']);
            }
            
            
        },
        error: function(){
            console.log('complete: error');
        }
    });
}
function WorkWithCities(cities) {
    var box = $('[data-name="city"]');
    var list = $(box).find('[data-city="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in cities){
        var itm = cities[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == true){
            form.city = itm['id'];
            $(box).find('[data-city="title"]').html(itm['title']);
        }
        $(list).append(clone);
    }
}

function RenewNightsList(parameters) {
    if(typeof(parameters) == 'object'){
        var nightsFromBox = $('[data-name="nightsFrom"]');
        var nightsToBox = $('[data-name="nightsTo"]');
        var nightsFromList = $(nightsFromBox).find('[data-nightsFrom="list"]');
        var nightsToList = $(nightsToBox).find('[data-nightsTo="list"]');
        $(nightsFromList).find('[data-tpl="item"]').remove();
        $(nightsToList).find('[data-tpl="item"]').remove();
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        for(var key in parameters){
            var itm = parameters[key];
            var nfClone = clone.clone(true);
            var ntClone = clone.clone(true);
            $(nfClone).click({type:'from',v:itm},NightsSelect);
            $(ntClone).click({type:'to',v:itm},NightsSelect);
            nfClone.attr('data-val',itm);
            nfClone.find('[data-box="title"]').html(itm);
            ntClone.attr('data-val',itm);
            ntClone.find('[data-box="title"]').html(itm);
            $(nightsFromList).append(nfClone.clone(true));
            $(nightsToList).append(ntClone.clone(true));
        }
    }
    return true;
}

function NightsSelect(parameters) {
    if(parameters.data.type == 'from'){
        form.nightsFrom = parameters.data.v;
    }
    if(parameters.data.type == 'to'){
        form.nightsTo = parameters.data.v;
    }
}

function RenewCalendarWithFlyDates(parameters) {
    
    calendar.picker({
        yellow: (parameters['yellow'])?parameters['yellow']:[],
        green: (parameters['normal'])?parameters['normal']:[],
        stop: (parameters['stopsale'])?parameters['stopsale']:[]
    },{},$('[data-name="dateFrom"], [data-name="dateTo"]'));
}

function RenewParameters(parameters) {
    if(parameters['defaultDateFrom']){
        calendar.setDate(parameters['defaultDateFrom'],$('[data-name="dateFrom"]'));
        form.dateFrom = parameters['defaultDateFrom'];
    }
    if(parameters['defaultDateTo']){
        calendar.setDate(parameters['defaultDateTo'],$('[data-name="dateTo"]'));
        form.dateTo = parameters['defaultDateTo'];
    }
    
    if(parameters['defaultFrom']){
        form.nightsFrom = parameters['defaultFrom'];
        $('[data-name="nightsFrom"]').find('[data-nightsFrom="title"]').html(parameters['defaultFrom']);
    }
    if(parameters['defaultTo']){
        form.nightsTo = parameters['defaultTo'];
        $('[data-name="nightsTo"]').find('[data-nightsTo="title"]').html(parameters['defaultTo']);
    }
}

function WorkWithSpo(spo) {
    
    var box = $('[data-name="spo"]');
    var list = $(box).find('[data-spo="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in spo){
        var itm = spo[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == true){
            form.spo = itm['id'];
            $(box).find('[data-spo="title"]').html(itm['title']);
        }
        $(list).append(clone);
    }
}
function WorkWithTourTypes(spo) {
    
    var box = $('[data-name="tourType"]');
    var list = $(box).find('[data-tourType="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in spo){
        var itm = spo[key];
        var clone = $('[data-template="CountryItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        if(itm['df'] == true){
            form.ttour = itm['id'];
            $(box).find('[data-tourType="title"]').html(itm['title']);
        }
        $(list).append(clone);
    }
    
    
}
function WorkWithResorts(resorts) {
    if(typeof(resorts) == 'object'){
        var MainBranchClone = $('[data-template="TreeFirstItem"]').find('[data-tpl="item"]').clone(true),
            SecondBranchClone = $('[data-template="TreeSecondItem"]').find('[data-tpl="item"]').clone(true),
            ResortBox = $('[data-name="Resorts"]'),
            ResortList = $('[data-name="Resorts"]').find('[data-resorts="list"]');
        for(var city in resorts){
            var cityItem = (typeof(resorts[city]) == 'object')?resorts[city]:false;
            if(cityItem != false){
                var MainBranch = $(MainBranchClone).clone(true);
                $(MainBranch).find('[data-box="title"]').html(cityItem['title']);
                $(MainBranch).find('[data-box="active"]').attr('data-val',cityItem['id']);
                
//                if( typeof(form.cities[cityItem['id']]) == 'undefined'){
//                    form.cities[cityItem['id']] = {
//                        'title' : cityItem['title'],ps : 0,list : {}
//                    };
//                }
                
                if( typeof(cityItem['resorts']) == 'object' ){
                    for(var resort in cityItem['resorts']){
                        var resortItem = (typeof(cityItem['resorts'][resort]) == 'object')?cityItem['resorts'][resort]:false;
                        if(resortItem != false){
                            var SecondBranch = $(SecondBranchClone).clone(true);
                            $(SecondBranch).find('[data-box="title"]').html(resortItem['title']);
                            $(SecondBranch).find('[data-box="active"]').attr('data-val',resortItem['id']);
                            form.resorts[resortItem['id']] = { ps : 0 };
//                            if( typeof(form.cities[cityItem['id']]['list'][resortItem['id']]) == 'undefined' ){
//                                form.cities[cityItem['id']]['list'][resortItem['id']] = {
//                                    'title' : resortItem['title'],ps : 0
//                                };
//                            }
                            
                            $(MainBranch).find('[data-box="list"]').append(SecondBranch);
                        }
                    }
                }
                
                $(ResortList).append(MainBranch);
            }
        }
        $(ResortBox).find('.tour-scrollbar').data("plugin_tinyscrollbar").update();
    }
}

function WorkWithHotels(hotels) {
    
    var box = $('[data-name="Hotels"]');
    var list = $(box).find('[data-hotels="list"]');
    $(list).find('[data-tpl="item"]').remove();
    for(var key in hotels){
        var itm = hotels[key];
        var clone = $('[data-template="HotelsItem"]').find('[data-tpl="item"]').clone(true);
        clone.attr('data-val',itm['id']);
        clone.find('[data-box="title"]').html(itm['title']);
        $(list).append(clone);
    }
    $(box).find('.tour-scrollbar').data("plugin_tinyscrollbar").update();
}